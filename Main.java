import AST.Statement.Expression.BinaryExpression.FuncExpression;
import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import AST.Statement.Type.*;
import IR.Quadruple;
import IR.TA;
import Parser.ClassNameListener;
import Parser.MisakaLexer;
import Parser.MisakaParser;
import Symbol.Symbol;
import Symbol.Table;
import org.antlr.v4.misc.MutableInt;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Scanner;
//import Symbol.Name;


import static Parser.ClassNameListener.*;
import static Symbol.Symbol.symbol;
import static Symbol.Table.levelfull;
import static java.lang.System.exit;

public class Main {


    public static void main(String[] args) throws Exception {

        InputStream is = System.in;
        //InputStream is = new FileInputStream("input.c"); // or System.in;
        ANTLRInputStream input = new ANTLRInputStream(is);

        ////System.out.println(input.toString());
        MisakaLexer lexer = new MisakaLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MisakaParser parser = new MisakaParser(tokens);
        ParseTree tree = parser.compilation_unit(); // compilation_unit is the starting rule of Misaka

        if (parser.getNumberOfSyntaxErrors() != 0) {
            throw new RuntimeException("shit");
        }

        levelfull = 0;
        iterlevel = 0;
        Mop = new Table();
        Mop.beginScope();


        tree.toStringTree(parser);
        /*
        //System.out.println("LISP:");
        //System.out.println(tree.toStringTree(parser));
        //System.out.println();
        */


        ////System.out.println("Listener:");
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(new ClassNameListener(), tree);
        //ClassNameListener.nodeProperty.get(tree).ASTprint();


        //Mop = new Table();
        //exit(1);
        /*
        //System.out.println();
        for (Enumeration e = Mop.keys(); e.hasMoreElements();) {
            //System.out.println(e.nextElement());
        }
        //System.out.println("------- CLASSEND -------");
        */

        /*
        //System.out.println();
        //System.out.println("------- AST -------");
        TreeRoot.ASTprint();
        //System.out.println("------- ASTEND -------");
        */

        ////System.out.println();
        ////System.out.println("------- Pre2 -------");
        TreeRoot.Pre2();
        /*
        for (Enumeration e = Mop.keys(); e.hasMoreElements();) {
            //System.out.println(e.nextElement());
        }
        */


        Type hs1, para;
        String name;
        MultiType para1, para2, para3;
        FuncExpression fe;

        name = "println";
        hs1 = new VoidType();
        para = new StringType();
        para1 = new MultiType(para);
        fe = new FuncExpression(hs1, name, para1);
        Mop.put(symbol(name), fe);
        FSN.put(symbol("func_println"), 0);

        name = "print";
        fe = new FuncExpression(hs1, name, para1);
        Mop.put(symbol(name), fe);
        FSN.put(symbol("func_print"), 0);

        name = "getString";
        hs1 = new StringType();
        fe = new FuncExpression(hs1, name, null);
        Mop.put(symbol(name), fe);
        FSN.put(symbol("func_getString"), 0);

        name = "getInt";
        hs1 = new IntType();
        fe = new FuncExpression(hs1, name, null);
        Mop.put(symbol(name), fe);
        FSN.put(symbol("func_getInt"), 0);

        name = "toString";
        hs1 = new StringType();
        para = new IntType();
        para1 = new MultiType(para);
        fe = new FuncExpression(hs1, name, para1);
        Mop.put(symbol(name), fe);
        FSN.put(symbol("func_toString"), 0);

        //System.out.println("------- Pre2END -------");
        //System.out.println();

        //System.out.println("------- Final -------");
        if (Mop.get(symbol("main")) == null) {
            //System.out.println("Main: no main here");
            throw new RuntimeException("");
        }
        if (Mop.get(symbol("main")) instanceof FuncExpression) {
            FuncExpression x = (FuncExpression)Mop.get(symbol("main"));
            if (x.ty instanceof IntType) {

            } else {
                //System.out.println("Main: main is not a int func");
                throw new RuntimeException("");
            }

            if (x.para == null) {

            } else {
                //System.out.println("Main: main can't have para");
                throw new RuntimeException("");
            }

        } else {
            //System.out.println("Main: main is not a func");
            throw new RuntimeException("");
        }


        Mop.beginScope();
        TreeRoot.Final();
        Mop.endScope();

        //System.out.println("------- FinalEND -------");
        //System.out.println();


        /*
        File out = new File("./LLIRInterpreter-master/src/input.ir");
        writer = new BufferedWriter(new FileWriter(out));
        */

        File out = new File("output.s");
        writer = new BufferedWriter(new FileWriter(out));

        //writer.append("Test" + split);

        //System.out.println("------- IR Start -------");
        ////System.out.println(writer.);
        //ArrayList<Quadruple> loc = new ArrayList<Quadruple>();

        Mop.beginScope();
        TA threeAddress = new TA();
        TreeRoot.IRB(threeAddress);
        Mop.endScope();

        /*
        TA类型：前IR类型，因为一些原因被重命名了。存储着函数list。每个函数有一个loc。
        IRB函数：返回值为void，以TA类型为参数，func及以上级别才拥有。以传参形式获得func列表。
        IR函数：func以下的语句会调用，只要是没有具体返回值的。以loc为参数，返回值为void。
        IRE函数：有返回值的表达式等才会使用。以loc为参数，返回值为Address。
         */



        //threeAddress.print();

        /*
        for(Iterator it2 = loc.iterator(); it2.hasNext();){
            //System.out.println(it2.next().toString());
        }
        */

        //System.out.println("------- IR End -------");

        File in = new File("build_in_functions.s");
        String tempString = null;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(in));
            int line = 1;
            while ((tempString = reader.readLine()) != null) {
                writer.append(tempString + "\n");
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }


        writer.append(".data\n");

        for(Iterator<Integer> it2 = FullSetList.iterator(); it2.hasNext();){
            writer.append("T" + it2.next().toString() + ": .space 4\n");
            //it2.next().setTemp();
        }

        String STS;
        int q = 0;
        for(Iterator<String> it2 = SaintTerror.iterator(); it2.hasNext();){
            STS = it2.next().toString();
            int ll = STS.length() - 2;
            String AA = "";
            char item;
            for (int i = 0; i < STS.length(); i++) {
                item =  STS.charAt(i);
                if (item != '\\') {
                    AA = AA + item;
                } else {
                    ll--;
                    i++;
                    item =  STS.charAt(i);
                    if (item == 'n') {
                        AA = AA + '\n';
                    } else if (item == '\\') {
                        AA = AA + '\\';
                    } else if (item == '"') {
                        AA = AA + '"';
                    }
                }
                //System.out.println(item);
            }

            writer.append(".word " + ll + "\n");
            writer.append("str" + q + ": .asciiz " + STS + "\n");
            q++;
            writer.append(".align 2\n");
            //it2.next().toString();
        }
        /*
        for (Iterator<Quadruple> it2 = fullvar.iterator(); it2.hasNext(); ) {

            it2.next().toMIPS();
            q++;
        }
        */

        writer.append(".text\n");

        threeAddress.toMIPS();

        //writer.append("_________________________________\n\n");
        //threeAddress.print();

        writer.flush();
        writer.close();


        File in2 = new File("output.s");
        String ts2 = null;
        String ts3 = null;
        tempString = null;
        reader = null;
        try {
            reader = new BufferedReader(new FileReader(in2));
            int line = 1;
            while ((tempString = reader.readLine()) != null) {
                ts3 = tempString;
                StringBuilder strb = new StringBuilder(ts3);

                if (ts3.length() > 4) strb.replace(0, 1, "s");
                ts3 = strb.toString();
                if (ts3.equals(ts2)) {

                } else {
                    System.out.println(tempString);
                }

                ts2 = tempString;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }

        Mop.endScope();
    }
}
