package Symbol;

import IR.Temp;

public class Symbol {
    private String name;
    private Symbol(String n) {
        name = n;
    }
    private Symbol(Temp n) {
        name = (n.num + "");
    }
    private Symbol(int n) {
        name = (n + "");
    }
    private static java.util.Dictionary dict = new java.util.Hashtable();

    public String toString() {
	    return name;
    }

   /**
    * Make return the unique symbol associated with a string.
    * Repeated calls to <tt>symbol("abc")</tt> will return the same Symbol.
    */

    public static Symbol symbol(String n) {
	    String u = n.intern();
	    Symbol s = (Symbol)dict.get(u);
	    if (s == null) {
		    s = new Symbol(u);
            //s.num = tempCount++;
		    dict.put(u,s);
	    }
	    return s;
    }

    public static Symbol symbol(Temp n) {
        String u = (n.num + "");
        Symbol s = (Symbol)dict.get(u);
        if (s == null) {
            s = new Symbol(u);
            //s.num = tempCount++;
            dict.put(u,s);
        }
        return s;
    }

    public static Symbol symbol(int n) {
        String u = (n + "");
        Symbol s = (Symbol)dict.get(u);
        if (s == null) {
            s = new Symbol(u);
            //s.num = tempCount++;
            dict.put(u,s);
        }
        return s;
    }
}

