package AST.DeclarationFull;

import AST.Statement.Expression.BinaryExpression.DeclFunc;
import AST.Statement.Expression.BinaryExpression.DeclFunc2;
import AST.Statement.Expression.BinaryExpression.DeclFunc3;
import AST.Statement.Expression.BinaryExpression.FuncExpression;
import AST.Statement.Expression.Expression;
import AST.Statement.Expression.UnaryExpression.ID;
import AST.Statement.Statement;
import AST.Statement.Type.MultiType;
import AST.Statement.Type.Type;
import IR.*;

import java.util.ArrayList;
import java.util.Iterator;

import static IR.Temp.tempCount;
import static Parser.ClassNameListener.Mop;
import static Parser.ClassNameListener.loc;
import static Parser.ClassNameListener.writer;
import static Symbol.Symbol.symbol;
import static Symbol.Table.levelfull;

public class FuncDeclaration implements DeclarationFull {
    Type hs1;
    Expression hs2;
    Statement hs3;
    String name;
    MultiType para;

    public FuncDeclaration(Type hs1, Expression hs2, Statement hs3) {
        this.hs1 = hs1;
        this.hs2 = hs2;
        this.hs3 = hs3;
    }
    public void ASTprint() {
        //System.out.println("Func");
        hs1.ASTprint();
        hs2.ASTprint();
        hs3.ASTprint();
    }
    public void Pre2() {
        //has things
        hs1.Pre2();
        if (hs2 instanceof DeclFunc) {
            Expression T = ((DeclFunc)hs2).uhs;
            if (T instanceof ID) {
                //System.out.println("Proper Func1");
                name = ((ID)T).name;

                //para.lhs = ty1.lhs;
                //para.rhs = ty1.rhs;
                FuncExpression fe;
                fe = new FuncExpression(hs1, name, null);
                Mop.put(symbol(name), fe);

                /*
                //System.out.println(((ID)T).name);
                Mop.put(symbol(name), this);
                */
            } else {
                //System.out.println("Not Proper Func1");
                throw new RuntimeException("");
            }
        } else if (hs2 instanceof DeclFunc2) {
            //System.out.println("Not Proper Func2");
            throw new RuntimeException("");
        } else if (hs2 instanceof DeclFunc3) {
            Expression T = ((DeclFunc3)hs2).lhs;
            if (T instanceof ID) {
                name = ((ID)T).name;
                //System.out.println("Proper Func3, name: " + ((ID)T).name);
                hs2.Pre2();
                para = new MultiType();
                MultiType ty1 = (MultiType)hs2.ty;
                para = ty1;
                //para.lhs = ty1.lhs;
                //para.rhs = ty1.rhs;
                FuncExpression fe;
                fe = new FuncExpression(hs1, name, para);
                Mop.put(symbol(name), fe);
            } else {
                //System.out.println("Not Proper Func3");
                throw new RuntimeException("");
            }

        }
    }
    public void Final() {
        //System.out.println("Func para");
        levelfull++;
        Mop.beginScope();
        hs2.Final();
        levelfull--;
        //System.out.println("Func stmt");
        hs3.Final();
        Mop.endScope();
    }

    public void IRB(TA ThreeAddress) throws Exception {
        loc = new ArrayList<Quadruple>();
        int pre = tempCount;
        levelfull++;
        Mop.beginScope();
        Address fp = null;
        fp = hs2.IRE();
        levelfull--;
        ////System.out.println("passed param");
        hs3.IR();
        Mop.endScope();

        ////System.out.println("passed param222");

        Function tasrc = new Function((FuncPart)fp, loc, (tempCount - pre) * 4);
        ThreeAddress.fragments.add(tasrc);
        //writer.append("}\n");
    }

    public void IR() throws Exception {

    }
}
