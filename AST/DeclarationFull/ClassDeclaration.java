package AST.DeclarationFull;

import AST.Statement.DeclarationStatement.Declaration;
import AST.Statement.Expression.UnaryExpression.ID;
import AST.Statement.MultiStatement.BlockItemList;
import AST.Statement.Statement;
import AST.Statement.Type.*;
import AST.Statement.UnaryStatement.CompoundStmt;
import AST.Statement.UnaryStatement.CompoundStmt2;
import IR.Quadruple;
import IR.TA;
import Symbol.Table;

import java.util.ArrayList;

import static Parser.ClassNameListener.Mop;
import static Symbol.Symbol.symbol;

public class ClassDeclaration implements DeclarationFull {
    public String name;
    public Statement rhs;

    public Table Vars = new Table();
    public int size = 0;
    public Table getVars() {
        return Vars;
    }

    public ClassDeclaration(String lhs, Statement rhs) {
        this.name = lhs;
        this.rhs = rhs;
        if (Mop.get(symbol(name)) == null) {
            //System.out.println("CLASS:"+lhs);
            Mop.put(symbol(name), this);
        } else {
            //System.out.println("WTF\n");
            throw new RuntimeException("");
        }
    }
    public void ASTprint() {
        //System.out.println("Class");
        //System.out.println(name);
        rhs.ASTprint();
    }
    public void Pre2() {
        //System.out.println("Pre2 class here:"+name);
        if (rhs instanceof CompoundStmt) {
            //System.out.println("Empty Class");
        } else if (rhs instanceof CompoundStmt2) {
            Statement T1 = ((CompoundStmt2)rhs).uhs;
            Statement T2;
            if (T1 instanceof BlockItemList) {
                T2 = ((BlockItemList)T1).rhs;
            } else {
                T2 = T1;
            }
            while (T2 instanceof Declaration) {
                //System.out.println("A var1");
                Declaration T3 = (Declaration)T2;
                if (T3.hs2 instanceof ID) {
                    String nm = ((ID)T3.hs2).name;
                    ID x = new ID(nm);
                    x.leftv = 1;
                    if (Vars.get(symbol(nm)) != null) {
                        //System.out.println("ClassVarReDefed1");
                        throw new RuntimeException("");
                    }
                    if (T3.hs1 instanceof DefType) {
                        String tn = ((DefType)(T3.hs1)).name;
                        if (Mop.get(symbol(tn)) == null) {
                            //System.out.println("Not defined Class1");
                            throw new RuntimeException("");
                        } else {
                            x.ty = T3.hs1;
                            Vars.put(symbol(nm), x, size);
                        }
                    } else {
                        x.ty = T3.hs1;
                        Vars.put(symbol(nm), x, size);
                    }
                    size += 4;
                } else {
                    //System.out.println("Not a right Class Var");
                    throw new RuntimeException("");
                }
                if (T1 instanceof Declaration) {
                    break;
                }
                T1 = ((BlockItemList)T1).lhs;
                if (T1 instanceof Declaration) {
                    T2 = T1;
                } else {
                    T2 = ((BlockItemList) T1).rhs;
                }
            }
            if (T2 instanceof Declaration) {

            } else {
                //System.out.println("Has wrong Class2");
                throw new RuntimeException("");
            }
        }
    }
    public void Final() {

    }

    public void IRB(TA ThreeAddress) throws Exception {

    }

    public void IR() throws Exception {

    }
}
