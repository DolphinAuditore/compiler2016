package AST.DeclarationFull;

import AST.Statement.Statement;
import AST.Statement.Type.Type;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public class VarDeclaration implements DeclarationFull {
    Type hs1;
    Statement hs2;
    Statement hs3;
    public VarDeclaration(Type hs1, Statement hs2, Statement hs3) {
        this.hs1 = hs1;
        this.hs2 = hs2;
        this.hs3 = hs3;
    }
    public void ASTprint() {
        //System.out.println("Func");
        hs1.ASTprint();
        hs2.ASTprint();
        hs3.ASTprint();
    }
    public void Pre2() {

    }
    public void Final() {
    }
    public void IRB(TA ThreeAddress) throws Exception {

    }

    public void IR() throws Exception {

    }
}
