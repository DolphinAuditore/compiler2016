package AST.Statement.DeclarationStatement;

import AST.Statement.Statement;

public class DeclArray2 extends DeclarationStatement {
    public Statement lhs, rhs;

    public DeclArray2(Statement lhs, Statement rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public void Final() {
        //
    }
}
