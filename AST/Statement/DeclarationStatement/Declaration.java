package AST.Statement.DeclarationStatement;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.Type;
import AST.Statement.Type.UnType;
import AST.Statement.Type.VoidType;
import IR.*;

import java.util.ArrayList;
import java.util.Iterator;

import static Parser.ClassNameListener.*;
import static Symbol.Symbol.symbol;

public class Declaration extends DeclarationStatement {
    public Type hs1;
    public Expression hs2;
    public Expression hs3;
    public static Type hsx = new UnType();

    public Declaration(Type hs1, Expression hs2, Expression hs3) {
        this.hs1 = hs1;
        this.hs2 = hs2;
        this.hs3 = hs3;
    }

    public void ASTprint() {
        //System.out.println("Decl");
        hs1.ASTprint();
        hs2.ASTprint();
        if (hs3 != null) hs3.ASTprint();
    }
    public void Pre2() {

    }

    public void Final() {
        hs1.Final();
        hsx = hs1;
        hs2.Final();
        hsx = new UnType();
        if (hs3 != null) hs3.Final();

        ////System.out.println("test declaration assign: " + hs3.ty.getClass().getSimpleName());

        if (hs3 != null) {
            if (hs2.ty.typeEq(hs3.ty) == 0) {
                //System.out.println("23 not agree");
                throw new RuntimeException("");
            } else {
                //System.out.println(hs2.ty.getClass().getSimpleName());
                //System.out.println(hs3.ty.getClass().getSimpleName());
                //System.out.println("23agree");
            }
        } else {
            //System.out.println("Declaration hs3 null, ok");
        }

        if (hs1.typeEq(hs2.ty) == 0) {
            //System.out.println("12 not agree");
            throw new RuntimeException("");
        } else if (hs1 instanceof VoidType) {
            //System.out.println("Declaration can't be void");
            throw new RuntimeException("");
        } else {
            //System.out.println("12agree");
        }

    }

    public void IR() throws Exception{
        hs1.Final();
        hsx = hs1;

        Address src1;
        src1 = hs2.IRE();

        hsx = new UnType();

        Address src2;
        if (hs3 != null) {
            src2 = hs3.IRE();
            loc.add(new Assign(src1, src2));
        }
    }

    public void IRB(TA ThreeAddress) throws Exception{
        loc = new ArrayList<Quadruple>();
        hs1.Final();
        hsx = hs1;

        Address src1;
        src1 = hs2.IRE();
        hsx = new UnType();

        FullSet.put(symbol(((Temp)src1).num), FS);
        FullSetList.add(FS);
        FS++;

        Address src2;
        if (hs3 != null) {
            src2 = hs3.IRE();
            loc.add(new Assign(src1, src2));
            for (Iterator<Quadruple> it2 = loc.iterator(); it2.hasNext(); ) {
                fullvar.add(it2.next());
                Extra += 4;
                //writer.append(it2.next().toString() + "\n");
            }
            //Function tasrc = new Function(null, src1, loc);
            //ThreeAddress.fragments.add(tasrc);
        }
    }

}
