package AST.Statement.DeclarationStatement;

import AST.Statement.Statement;

public class DeclArray extends DeclarationStatement {
    public Statement uhs;

    public DeclArray(Statement uhs) {
        this.uhs = uhs;
    }

    public void ASTprint() {
        uhs.ASTprint();
    }
}
