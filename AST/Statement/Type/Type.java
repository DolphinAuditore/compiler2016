package AST.Statement.Type;

import AST.Statement.Statement;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public class Type implements Statement {

    public void ASTprint() {

    }
    public void Pre2() {}
    public void Final() {
    }

    public int typeEq(Type a) {
        if (this.getClass() == a.getClass()) {
            if (this instanceof ArrayType) {
                ArrayType x, y;
                x = (ArrayType)this;
                y = (ArrayType)a;
                if (x.lhs.typeEq(y.lhs) == 1 && x.rhs == y.rhs) {
                    return 1;
                } else {
                    return 0;
                }
            } else if (this instanceof DefType) {
                DefType x, y;
                x = (DefType)this;
                y = (DefType)a;
                if (x.name.equals(y.name)) {
                    return 1;
                } else {
                    return 0;
                }
            } else if (this instanceof MultiType) {
                MultiType x, y;
                x = (MultiType)this;
                y = (MultiType)a;
                if (x.lhs.typeEq(y.lhs) == 0) {
                    return 0;
                }
                if (x.rhs != null) {
                    if (y.rhs != null) {
                        return x.rhs.typeEq(y.rhs);
                    } else {
                        return 0;
                    }
                } else if (y.rhs != null) {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        } else if(a instanceof NullType && (this instanceof ArrayType || this instanceof DefType)) {
            return 1;
        } else {
            return 0;
        }
    }
    public void IRB(TA ThreeAddress) throws Exception {

    }
    public void IR() throws Exception {

    }
}
