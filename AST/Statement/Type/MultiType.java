package AST.Statement.Type;

import AST.Statement.Expression.Expression;

public class MultiType extends Type {

    public Type lhs;
    public MultiType rhs;

    public MultiType(Type lhs, MultiType rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        /*
        if (lhs == null) {
            //System.out.println("ASH!");
        }*/
    }

    public MultiType(Type lhs) {
        this.lhs = lhs;
        /*
        if (lhs == null) {
            //System.out.println("ASH!2");
        }*/
    }

    public MultiType() {

    }

    public void ASTprint() {

    }

    public void Pre2() {

    }


}
