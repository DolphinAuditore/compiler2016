package AST.Statement.Type;

import AST.DeclarationFull.ClassDeclaration;
import Symbol.Table;

import static Parser.ClassNameListener.Mop;
import static Symbol.Symbol.symbol;
import static java.lang.System.exit;

public class DefType extends Type {

    public String name;
    public Table Vars;
    public Table getVars() {
        return Vars;
    }
    public int size;

    public DefType(String name) {
        this.name = name;
    }

    public void ASTprint() {
        //System.out.println(name);
    }

    public void Pre2() {
        if (Mop.get(symbol(name)) instanceof ClassDeclaration) {
        } else {
            //System.out.println("Undefed type");
            throw new RuntimeException("");
        }
    }

    public void Final() {
        if (Mop.get(symbol(name)) instanceof ClassDeclaration) {
            Vars = ((ClassDeclaration) Mop.get(symbol(name))).getVars();
            size = ((ClassDeclaration) Mop.get(symbol(name))).size;
        } else {
            //System.out.println(name);
            //System.out.println("Undefed type");
            throw new RuntimeException("");
        }
    }
}
