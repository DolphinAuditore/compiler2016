package AST.Statement.Type;

import AST.Statement.Expression.Expression;

import static Parser.ClassNameListener.Mop;
import static Symbol.Symbol.symbol;
import static java.lang.System.exit;

public class ArrayType extends Type {

    public Type lhs;
    public int  rhs;

    public ArrayType() {
        lhs = new UnType();
        rhs = 0;
    }

    public ArrayType(Type lhs) {
        if (lhs instanceof ArrayType) {
            this.rhs = ((ArrayType)lhs).rhs + 1;
            this.lhs = ((ArrayType)lhs).lhs;
        } else {
            this.lhs = lhs;
            this.rhs = 1;
        }
    }

    public void ASTprint() {

    }

    public void Pre2() {
        if (lhs instanceof DefType) {
            DefType x = (DefType)lhs;
            if (Mop.get(symbol(x.name)) == null) {
                //System.out.println("para array defType wrong");
                throw new RuntimeException("");
            }
        }
    }

}
