package AST.Statement.MultiStatement;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import AST.Statement.Type.BoolType;
import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.loc;

public class SelectDef2 implements Statement {
    public Expression hs1;
    public Statement hs2, hs3;

    public SelectDef2(Expression hs1, Statement hs2, Statement hs3) {
        this.hs1 = hs1;
        this.hs2 = hs2;
        this.hs3 = hs3;
    }

    public void ASTprint() {
        hs1.ASTprint();
        hs2.ASTprint();
        hs3.ASTprint();
    }

    public void Pre2() {}
    public void Final() {
        hs1.Final();
        hs2.Final();
        hs3.Final();
        if (hs1.ty instanceof BoolType) {

        } else {
            //System.out.println("if2: ye shang tian");
            throw new RuntimeException("");
        }
    }
    public void IR() throws Exception{
        Address cond = hs1.IRE();
        Label lab1 = new Label();
        Label lab2 = new Label();
        Label lab3 = new Label();
        JumpEnd end1 = new JumpEnd(lab1);
        JumpEnd end2 = new JumpEnd(lab2);
        Jump to1 = new Jump(lab1);
        Jump to3 = new Jump(lab3);
        JumpEnd end3 = new JumpEnd(lab3);

        Branch De = new Branch(cond, lab1, lab2);
        loc.add(De);
        loc.add(end1);
        hs2.IR();
        loc.add(to3);
        loc.add(end2);
        hs3.IR();
        loc.add(to3);
        loc.add(end3);
    }

    public void IRB(TA ThreeAddress) throws Exception {

    }

}


