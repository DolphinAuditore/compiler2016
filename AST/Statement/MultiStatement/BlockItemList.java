package AST.Statement.MultiStatement;

import AST.Statement.Statement;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public class BlockItemList implements Statement {
    public Statement lhs, rhs;

    public BlockItemList(Statement lhs, Statement rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public void Pre2() {}
    public void Final() {
        lhs.Final();
        rhs.Final();
    }
    public void IR() throws Exception{
        lhs.IR();
        rhs.IR();
    }

    public void IRB(TA ThreeAddress) throws Exception {

    }
}
