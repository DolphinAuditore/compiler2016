package AST.Statement.MultiStatement;

import AST.Statement.Statement;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public class ParaDeclDef implements Statement {
    public Statement lhs, rhs;

    public ParaDeclDef(Statement lhs, Statement rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public void Pre2() {}
    public void Final() {}
    public void IRB(TA ThreeAddress) throws Exception {

    }

    public void IR() throws Exception {

    }
}
