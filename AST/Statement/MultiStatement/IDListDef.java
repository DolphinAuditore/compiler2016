package AST.Statement.MultiStatement;

import AST.Statement.Statement;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public class IDListDef implements Statement {
    public Statement lhs, rhs;

    public IDListDef(Statement lhs, Statement rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public void Pre2() {}
    public void Final() {}
    public void IR() throws Exception{
    }
    public void IRB(TA ThreeAddress) throws Exception {

    }

}
