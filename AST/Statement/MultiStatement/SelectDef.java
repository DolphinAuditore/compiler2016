package AST.Statement.MultiStatement;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import AST.Statement.Type.BoolType;
import IR.*;
import jdk.nashorn.internal.ir.JumpToInlinedFinally;

import java.util.ArrayList;

import static Parser.ClassNameListener.loc;

public class SelectDef implements Statement {
    public Expression lhs;
    public Statement rhs;

    public SelectDef(Expression lhs, Statement rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public void Pre2() {}
    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty instanceof BoolType) {

        } else {
            //System.out.println("if: shang tian");
            throw new RuntimeException("");
        }
    }
    public void IR() throws Exception {
        Address cond = lhs.IRE();
        Label lab1 = new Label();
        Label lab2 = new Label();

        JumpEnd end1 = new JumpEnd(lab1);
        JumpEnd end2 = new JumpEnd(lab2);
        Jump to1 = new Jump(lab1);
        Jump to2 = new Jump(lab2);

        Branch De = new Branch(cond, lab1, lab2);
        loc.add(De);
        loc.add(end1);
        rhs.IR();
        loc.add(to2);
        loc.add(end2);
    }

    public void IRB(TA ThreeAddress) throws Exception {

    }
}

