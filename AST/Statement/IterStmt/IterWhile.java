package AST.Statement.IterStmt;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import AST.Statement.Type.BoolType;
import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.*;

public class IterWhile extends IterStatement {
    public Expression lhs;
    public Statement rhs;

    public IterWhile(Expression lhs, Statement rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public void Final() {
        iterlevel++;
        lhs.Final();
        rhs.Final();
        if (lhs.ty instanceof BoolType) {

        } else {
            //System.out.println("while: wo ye shang tian");
            throw new RuntimeException("");
        }
        iterlevel--;
    }

    public void IR() throws Exception {
        Label lab1 = new Label();
        Label lab2 = new Label();
        Label lab3 = new Label();
        JumpEnd end1 = new JumpEnd(lab1);
        JumpEnd end2 = new JumpEnd(lab2);
        JumpEnd end3 = new JumpEnd(lab3);
        Jump to1 = new Jump(lab1);

        iterlevel++;
        ////System.out.println("it1 " + iterlevel);
        loc.add(to1);

        iterstart.add(lab1);
        iterend.add(lab3);
        //iterlast = iterstart.size() - 1;

        loc.add(end1);
        Address cond = lhs.IRE();
        Branch De = new Branch(cond, lab2, lab3);
        loc.add(De);
        loc.add(end2);
        rhs.IR();
        loc.add(to1);
        loc.add(end3);

        iterlevel--;
        iterstart.remove(lab1);
        iterend.remove(lab3);
    }
}
