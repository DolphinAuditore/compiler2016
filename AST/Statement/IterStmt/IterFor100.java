package AST.Statement.IterStmt;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.*;

public class IterFor100 extends IterStatement {
    public Expression hs1;
    public Statement hs4;

    public IterFor100(Expression hs1, Statement hs4) {
        this.hs1 = hs1;
        this.hs4 = hs4;
    }

    public void ASTprint() {
        hs1.ASTprint();
        hs4.ASTprint();
    }

    public void Final() {
        iterlevel++;
        hs1.Final();
        hs4.Final();
        iterlevel--;
    }

    public void IR() throws Exception {

        //the first expr begin
        hs1.IRE();
        //the first expr end

        Label lab1 = new Label();
        Label lab2 = new Label();
        Label lab3 = new Label();
        JumpEnd end1 = new JumpEnd(lab1);
        JumpEnd end2 = new JumpEnd(lab2);
        JumpEnd end3 = new JumpEnd(lab3);
        Jump to1 = new Jump(lab1);
        loc.add(to1);

        iterlevel++;
        iterstart.add(lab1);
        iterend.add(lab3);

        loc.add(end1);

        //stmt begin
        hs4.IR();
        //stmt end

        loc.add(to1);
        loc.add(end3);

        iterlevel--;
        iterstart.remove(lab1);
        iterend.remove(lab3);
    }
}
