package AST.Statement.IterStmt;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import AST.Statement.Type.BoolType;
import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.*;

public class IterFor010 extends IterStatement {
    public Expression hs2;
    public Statement hs4;

    public IterFor010(Expression hs2, Statement hs4) {
        this.hs2 = hs2;
        this.hs4 = hs4;
    }

    public void ASTprint() {
        hs2.ASTprint();
        hs4.ASTprint();
    }

    public void Final() {
        iterlevel++;
        hs2.Final();
        hs4.Final();
        if (hs2.ty instanceof BoolType) {

        } else {
            //System.out.println("for: yi qi shang tian");
            throw new RuntimeException("");
        }
        iterlevel--;
    }

    public void IR() throws Exception {

        Label lab1 = new Label();
        Label lab2 = new Label();
        Label lab3 = new Label();
        JumpEnd end1 = new JumpEnd(lab1);
        JumpEnd end2 = new JumpEnd(lab2);
        JumpEnd end3 = new JumpEnd(lab3);
        Jump to1 = new Jump(lab1);
        loc.add(to1);

        iterlevel++;
        iterstart.add(lab1);
        iterend.add(lab3);

        loc.add(end1);

        //the second expr begin
        Address cond = hs2.IRE();
        Branch De = new Branch(cond, lab2, lab3);
        loc.add(De);
        loc.add(end2);
        //the second expr end

        //stmt begin
        hs4.IR();
        //stmt end

        loc.add(to1);
        loc.add(end3);

        iterlevel--;
        iterstart.remove(lab1);
        iterend.remove(lab3);
    }
}
