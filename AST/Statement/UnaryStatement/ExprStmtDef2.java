package AST.Statement.UnaryStatement;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.loc;

public class ExprStmtDef2 implements Statement {
    Expression uhs;
    public ExprStmtDef2(Expression uhs) {
        this.uhs = uhs;
    }

    public void ASTprint() {
        uhs.ASTprint();
    }

    public void Pre2() {uhs.Pre2();}
    public void Final() {uhs.Final();}
    public void IRB(TA ThreeAddress) throws Exception {

    }
    public void IR() throws Exception{
        ////System.out.println("Reach IRE exprstmt");
        Address De = null;
        De = uhs.IRE();
        if (De instanceof CallExpr) {
            loc.add(new CallStmt((CallExpr)De));
        }
    }
    public Address IRE(ArrayList<Quadruple> loc) throws Exception{
        Address De = null;
        return De;
    }
}
