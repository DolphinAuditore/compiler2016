package AST.Statement.UnaryStatement;

import AST.Statement.Statement;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public class CompoundStmt implements Statement {
    public CompoundStmt(){}

    public void ASTprint() {
        //System.out.println("{}");
    }

    public void Pre2() {}
    public void Final() {}
    public void IRB(TA ThreeAddress) throws Exception {

    }

    public void IR() throws Exception {

    }
}
