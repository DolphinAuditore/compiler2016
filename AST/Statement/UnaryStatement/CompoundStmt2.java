package AST.Statement.UnaryStatement;

import AST.Statement.Statement;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

import static Parser.ClassNameListener.Mop;

public class CompoundStmt2 implements Statement {
    public Statement uhs;

    public CompoundStmt2(Statement uhs) {
        this.uhs = uhs;
    }

    public void ASTprint() {
        uhs.ASTprint();
    }

    public void Pre2() {}
    public void Final() {
        //levelfull++;
        Mop.beginScope();
        uhs.Final();
        Mop.endScope();
    }

    public void IR() throws Exception{
        Mop.beginScope();
        uhs.IR();
        Mop.endScope();
    }

    public void IRB(TA ThreeAddress) throws Exception {

    }

}
