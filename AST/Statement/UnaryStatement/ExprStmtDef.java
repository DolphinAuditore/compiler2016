package AST.Statement.UnaryStatement;

import AST.Statement.Statement;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public class ExprStmtDef implements Statement {

    public ExprStmtDef() {

    }

    public void ASTprint() {

    }

    public void Pre2() {}
    public void Final() {}
    public void IRB(TA ThreeAddress) throws Exception {

    }

    public void IR() throws Exception {

    }
}
