package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Expression.UnaryExpression.ID;
import AST.Statement.Type.*;
import IR.Address;
import IR.MultiT;
import IR.Quadruple;

import java.util.ArrayList;

import static AST.Statement.DeclarationStatement.Declaration.hsx;
import static Parser.ClassNameListener.Mop;
import static Symbol.Symbol.symbol;

public class ParaDecl extends Expression {
    public Type lhs;
    public Expression rhs;

    public ParaDecl(Type lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public String getName() {
        return name;
    }

    public void Pre2() {
        ty = new MultiType();
        if (lhs instanceof ArrayType || lhs instanceof DefType) {
            lhs.Pre2();
        }
        ((MultiType)ty).lhs = lhs;

        //System.out.println("*** : " + lhs.getClass().getSimpleName());
        //lhs.Pre2();
        //prhs.Pre2();
    }

    public void Final() {
        lhs.Final();
        hsx = lhs;
        rhs.Final();
        hsx = new UnType();

        if (lhs.typeEq(rhs.ty) == 0) {
            //System.out.println("para 12 not agree");
            throw new RuntimeException("");
        } else if (lhs instanceof VoidType) {
            //System.out.println("paraDeclaration can't be void");
            throw new RuntimeException("");
        } else {
            //System.out.println("para 12 agree");
        }
        /*
        rhs.Final();
        if (rhs instanceof ID) {
            Mop.put(symbol(rhs.name), );
        } else {
            //System.out.println("ParaDecl:Why this guy is not ID");
        }
        */
    }

    public Address IRE() throws Exception {
        hsx = lhs;
        Address De = rhs.IRE();
        hsx = new UnType();
        return De;
    }

}
