package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import AST.Statement.Type.IntType;
import IR.Address;
import IR.ArithmeticExpr;
import IR.Quadruple;
import IR.Temp;

import java.util.ArrayList;

import static IR.ArithmeticOp.add;
import static IR.ArithmeticOp.div;
import static Parser.ClassNameListener.loc;

public class MultiDevExpression extends Expression {
    public Expression lhs, rhs;

    public MultiDevExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new IntType();
    }

    public void ASTprint() {
        //System.out.println("Dev");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }
    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof IntType) {
                ty = lhs.ty;
            } else {
                //System.out.println("dev: un def");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("dev: Type Not Agree");
            throw new RuntimeException("");
        }
    }
    public Address IRE() throws Exception {
        Temp De = new Temp();
        ArithmeticExpr Ge = new ArithmeticExpr(De, lhs.IRE(), div, rhs.IRE());
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
    }
}
