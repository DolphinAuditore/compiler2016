package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import AST.Statement.Type.IntType;
import AST.Statement.Type.NullType;
import AST.Statement.Type.StringType;
import IR.Address;
import IR.Quadruple;
import IR.RelationalExpr;
import IR.Temp;

import java.util.ArrayList;

import static IR.RelationalOp.sge;
import static IR.RelationalOp.sne;
import static Parser.ClassNameListener.loc;

public class EqualNotExpression extends Expression {
    public Expression lhs, rhs;

    public EqualNotExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new BoolType();
    }

    public void ASTprint() {
        //System.out.println("NEqual");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof IntType) {
                ty = new BoolType();
            } else if (lhs.ty instanceof BoolType) {
                ty = new BoolType();
            } else if (rhs.ty instanceof NullType) {
                ty = new BoolType();
            } else {
                //System.out.println("equalnot: un def");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("equalnot: Type Not Agree");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        RelationalExpr Ge = new RelationalExpr(De, lhs.IRE(), sne, rhs.IRE());
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
    }
}
