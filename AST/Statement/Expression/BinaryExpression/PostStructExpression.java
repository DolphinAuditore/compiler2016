package AST.Statement.Expression.BinaryExpression;

import AST.DeclarationFull.ClassDeclaration;
import AST.Statement.Expression.Expression;
import AST.Statement.Expression.UnaryExpression.ID;
import AST.Statement.Statement;
import AST.Statement.Type.ArrayType;
import AST.Statement.Type.DefType;
import AST.Statement.Type.IntType;
import AST.Statement.Type.StringType;
import IR.*;
import Symbol.Symbol;
import Symbol.Table;

import java.util.ArrayList;
import java.util.Enumeration;

import static Parser.ClassNameListener.Mop;
import static Parser.ClassNameListener.loc;
import static Symbol.Symbol.symbol;

public class PostStructExpression extends Expression {
    public Expression lhs;
    public Expression rhs;
    public Table Mopback;

    public PostStructExpression(Expression lhs, ID rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        name = rhs.name;
        ty = rhs.ty;
    }

    public void ASTprint() {
        //System.out.println("PostStructExpr");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        lhs.Final();
        if (lhs.ty instanceof DefType) {
            Mopback = Mop;

            Mop = ((ClassDeclaration)Mopback.get(symbol(((DefType) lhs.ty).name))).getVars();

            rhs.Final();

            Mop = Mopback;
            ty = rhs.ty;
            leftv = rhs.leftv;

        } else if (lhs.ty instanceof StringType) {

            if (rhs instanceof ID) {
                String nm = rhs.getName();
                if (nm.equals("substring")) {
                    ty = new StringType();
                } else if (nm.equals("ord")) {
                    ty = new IntType();
                } else if (nm.equals("length")) {
                    ty = new IntType();
                } else if (nm.equals("parseInt")) {
                    ty = new IntType();
                } else {
                    //System.out.println("Not Proper string func1");
                    throw new RuntimeException("");
                }
            } else{
                //System.out.println("Not Valid String Func");
                throw new RuntimeException("");
            }
        } else if (lhs.ty instanceof ArrayType) {
            if (rhs.getName().equals("size")) {
                ty = new IntType();
            }
            /*
            //System.out.println("rhs: " + rhs.getClass().getSimpleName());

            if (rhs instanceof DeclFunc) {
                String nm = (((DeclFunc)rhs).uhs).getName();
                if (nm.equals("size")) {
                    ty = new IntType();
                } else {
                    //System.out.println("Not Proper array func1");
                    throw new RuntimeException("");
                }
            } else {
                //System.out.println("Not Valid array Func");
                throw new RuntimeException("");
            }
            */
        } else {
            //System.out.println("This type can't have post struct");
            throw new RuntimeException("");
        }
    }

    public Address IRL() throws Exception {
        Address src;
        int offset;

        src = lhs.IRE();
        ////System.out.println("OFFALL LL??");
        if (lhs.ty instanceof DefType) {
            Mopback = Mop;
            Mop = ((ClassDeclaration)Mopback.get(symbol(((DefType) lhs.ty).name))).getVars();
            offset = Mop.getoffset(symbol(rhs.getName()));
            Mop = Mopback;
            ty = rhs.ty;
            leftv = rhs.leftv;
            ////System.out.println("OFFALL LL4: " + offset);
        } else {
            offset = 0;
        }
        /*
        else if (lhs.ty instanceof StringType) {
        } else if (lhs.ty instanceof ArrayType) {
        }*/

        if (src instanceof Memory) {
            Temp src2 = new Temp();
            loc.add(new MemoryLoad((Memory)src, src2));
            src = src2;
        }

        Memory De = new Memory(src, offset);
        return De;
    }

    public Address IRE() throws Exception {
        Address src;
        int offset;

        src = lhs.IRE();
        ////System.out.println("OFFALL??");
        if (lhs.ty instanceof DefType) {
            Mopback = Mop;
            Mop = ((ClassDeclaration) Mopback.get(symbol(((DefType) lhs.ty).name))).getVars();
            offset = Mop.getoffset(symbol(rhs.getName()));
            Mop = Mopback;
            ty = rhs.ty;
            leftv = rhs.leftv;
            ////System.out.println("OFFALL 4: " + offset);
        } else if (lhs.ty instanceof ArrayType) {
            if (name.equals("size")) {
                Temp De = new Temp();
                loc.add(new MemoryLoad(new Memory(src, 0), De));
                return De;
            }
            offset = 0;
        } else {
            offset = 0;
        }

        if (src instanceof Memory) {
            Temp src2 = new Temp();
            loc.add(new MemoryLoad((Memory)src, src2));
            src = src2;
        }

        ////System.out.println("OFFALL 44: " + offset);
        Memory De = new Memory(src, offset);

        Temp srcx = new Temp();
        loc.add(new MemoryLoad(De, srcx));
        ////System.out.println("OFFALL 444: " + De.offset);
        return srcx;
        //return De;
    }
}
