package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;
import AST.Statement.Type.MultiType;
import AST.Statement.Type.Type;
import AST.Statement.Type.VoidType;
import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.Mop;
import static Parser.ClassNameListener.loc;
import static Symbol.Symbol.symbol;

public class PostArgumentExpression2 extends Expression {
    public Expression lhs, rhs;
    public Temp ph1, ph2;
    public PostArgumentExpression2(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        //System.out.println("PostArguExpr2");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs instanceof PostStructExpression) {
            if (lhs.getName().equals("substring")) {
                IntType a1 = new IntType();
                IntType a2 = new IntType();
                MultiType b2 = new MultiType(a2);
                MultiType b1 = new MultiType(a1, b2);
                if (rhs.ty.typeEq(b1) == 1) {
                    //System.out.println("postArgu2: substring func para match, ok");
                    ty = lhs.ty;
                } else {
                    //System.out.println("postArgu2: func para not match");
                    throw new RuntimeException("");
                }
            } else if (lhs.getName().equals("ord")) {
                IntType a1 = new IntType();
                MultiType b1 = new MultiType(a1);
                ////System.out.println("HHHHHHHHHHHHHHHHH\n");
                if (rhs.ty.typeEq(b1) == 1) {
                    //System.out.println("postArgu2: substring func para match, ok");
                    ty = lhs.ty;
                } else {
                    //System.out.println("postArgu2: func para not match");
                    throw new RuntimeException("");
                }
            } else {
                //System.out.println("postArgu2: not a Strfunc");
                throw new RuntimeException("");
            }
        } else if (Mop.get(symbol(lhs.getName())) instanceof FuncExpression) {
            FuncExpression x = (FuncExpression)Mop.get(symbol(lhs.getName()));
            //System.out.println("func: " + x.getClass().getSimpleName());
            //System.out.println("para: " + rhs.getClass().getSimpleName());
            //System.out.println("para 1st son: " + rhs.ty.getClass().getSimpleName());
            //System.out.println("para 1st sonson: " + ((MultiType)rhs.ty).lhs.getClass().getSimpleName());
            if (x.para.typeEq(rhs.ty) == 1) {
                ty = lhs.ty;
                //System.out.println("postArgu2: func para match, ok");
            } else {
                //System.out.println("postArgu2: func para not match");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("postArgu2: not a func");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        //uhs.Final();
        name = lhs.getName();

        ////System.out.println("NAAAAAAAAAAAAAAAAAAAAAME " + name);
        Address De = null;
        if (lhs instanceof PostStructExpression) {
            if (name.equals("ord")) {
                lhs.IRE();
                name = "func_string.ord";
                Address src1 = ((PostStructExpression)lhs).lhs.IRE();
                De = new CallExpr(name, new MultiT(src1, rhs.IRE()));
                Temp src = new Temp();
                loc.add(new CallStmt((CallExpr)De, src));
                return src;
            } else if (name.equals("substring")) {
                name = "func_string.substring";
                Temp DD;
                Address src1 = ((PostStructExpression)lhs).lhs.IRE();

                /*
                if (src1 instanceof StringConst) {
                    DD = new Temp();
                    loc.add(new Assign(DD, src1));
                } else {
                    DD = (Temp)src1;
                }
                De = new CallExpr(name, new MultiT(DD, rhs.IRE()));
                */

                De = new CallExpr(name, new MultiT(src1, rhs.IRE()));
                Temp src = new Temp();
                loc.add(new CallStmt((CallExpr)De, src));
                return src;
            }
        } else {
            lhs.IRE();
            if (Mop.get(symbol(name)) instanceof FuncExpression) {
                FuncExpression x = (FuncExpression)Mop.get(symbol(name));
                if (name.equals("print")) {
                    name = "func_print";
                } else if (name.equals("println")) {
                    name = "func_println";
                } else if (name.equals("toString")) {
                    name = "func_toString";
                } else {
                    name = name + 44;
                }

                De = new CallExpr(name, rhs.IRE());
                Type TT = ((FuncExpression)Mop.get(symbol(lhs.getName()))).ty;
                if (TT instanceof VoidType) {

                } else {
                    Temp src = new Temp();
                    loc.add(new CallStmt((CallExpr)De, src));
                    return src;
                }
            } else {
                //System.out.println("postArgu: not a func");
                throw new RuntimeException("");
            }
        }
        return De;
    }
}
