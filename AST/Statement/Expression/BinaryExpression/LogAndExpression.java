package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import AST.Statement.Type.IntType;
import AST.Statement.Type.StringType;
import IR.*;

import java.util.ArrayList;

import static IR.ArithmeticOp.xor;
import static IR.LogicalOp.and;
import static Parser.ClassNameListener.loc;

public class LogAndExpression extends Expression {
    public Expression lhs, rhs;

    public LogAndExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new BoolType();
    }

    public void ASTprint() {
        //System.out.println("LogAnd");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }
    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof BoolType) {
                ty = lhs.ty;
            } else {
                //System.out.println("logand: un def");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("logand: Type Not Agree");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        Assign Ae = new Assign(De, lhs.IRE());
        loc.add(Ae);

        Label lab1 = new Label();
        Label lab2 = new Label();
        JumpEnd end1 = new JumpEnd(lab1);
        JumpEnd end2 = new JumpEnd(lab2);
        Jump to1 = new Jump(lab1);
        Jump to2 = new Jump(lab2);

        Branch Be = new Branch(De, lab1, lab2);
        loc.add(Be);

        loc.add(end1);
        LogicalExpr Ge = new LogicalExpr(De, De, and, rhs.IRE());
        loc.add(Ge);
        loc.add(to2);

        loc.add(end2);
        return De;
        /*
        Address cond = lhs.IRE();
        Label lab1 = new Label();
        Label lab2 = new Label();

        JumpEnd end1 = new JumpEnd(lab1);
        JumpEnd end2 = new JumpEnd(lab2);
        Jump to1 = new Jump(lab1);
        Jump to2 = new Jump(lab2);

        Branch De = new Branch(cond, lab1, lab2);
        loc.add(De);
        loc.add(end1);
        rhs.IR();
        loc.add(to2);
        loc.add(end2);
        */
        ////System.out.println("Reach IRE ADD");

    }
}
