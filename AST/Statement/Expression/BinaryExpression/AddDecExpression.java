package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;
import AST.Statement.Type.StringType;
import IR.Address;
import IR.ArithmeticExpr;
import IR.Quadruple;
import IR.Temp;

import java.util.ArrayList;

import static IR.ArithmeticOp.add;
import static IR.ArithmeticOp.sub;
import static Parser.ClassNameListener.loc;

public class AddDecExpression extends Expression {
    public Expression lhs, rhs;

    public AddDecExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new IntType();
    }

    public void ASTprint() {
        //System.out.println("Dec");
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public String getName() {
        return name;
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof IntType) {
                ty = lhs.ty;
            } else {
                //System.out.println("adddec: can't dec it");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("adddec: Type Not Agree");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        ArithmeticExpr Ge = new ArithmeticExpr(De, lhs.IRE(), sub, rhs.IRE());
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
    }
}
