package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.MultiType;
import AST.Statement.Type.UnType;
import IR.Address;
import IR.Quadruple;

import java.util.ArrayList;

import static AST.Statement.DeclarationStatement.Declaration.hsx;

public class ArgumentDef extends Expression {
    public Expression uhs;

    public ArgumentDef(Expression uhs) {
        this.uhs = uhs;
    }

    public void ASTprint() {
        uhs.ASTprint();
    }

    public void Pre2() {}
    public void Final() {
        uhs.Final();
        /*
        if (uhs.ty == null) {
            //System.out.println("ASH!3");
            //System.out.println("ASH!4" + uhs.getClass().getSimpleName());
        }*/
        ty = new MultiType(uhs.ty);

    }
    public String getName() {
        return name;
    }

    public Address IRE() throws Exception {
        Address De = uhs.IRE();
        return De;
    }
}
