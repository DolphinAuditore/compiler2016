package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import AST.Statement.Type.IntType;
import IR.Address;
import IR.ArithmeticExpr;
import IR.Quadruple;
import IR.Temp;

import java.util.ArrayList;

import static IR.ArithmeticOp.sll;
import static IR.ArithmeticOp.xor;
import static Parser.ClassNameListener.loc;

public class ShiftLeftExpression extends Expression {
    public Expression lhs, rhs;

    public ShiftLeftExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new IntType();
    }

    public void ASTprint() {
        //System.out.println("ShiftLeft");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof IntType) {
                ty = lhs.ty;
            } else {
                //System.out.println("shiftleft: un def");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("shiftleft: Type Not Agree");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        ArithmeticExpr Ge = new ArithmeticExpr(De, lhs.IRE(), sll, rhs.IRE());
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
    }
}
