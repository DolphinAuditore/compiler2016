package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;
import AST.Statement.Type.StringType;
import IR.Address;
import IR.ArithmeticExpr;
import IR.Quadruple;
import IR.Temp;

import java.util.ArrayList;

import static IR.ArithmeticOp.and;
import static IR.ArithmeticOp.mul;
import static Parser.ClassNameListener.loc;

public class AndExpression extends Expression {
    public Expression lhs, rhs;

    public AndExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new IntType();
    }

    public void ASTprint() {
        //System.out.println("And");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof IntType) {
                ty = lhs.ty;
            } else {
                //System.out.println("and: need to and int");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("and: Type Not Agree");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        ArithmeticExpr Ge = new ArithmeticExpr(De, lhs.IRE(), and, rhs.IRE());
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
    }
}
