package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.ArrayType;
import AST.Statement.Type.NullType;
import AST.Statement.Type.UnType;
import IR.*;

import java.lang.management.MemoryType;
import java.util.ArrayList;

import static Parser.ClassNameListener.Mop;
import static Parser.ClassNameListener.loc;
import static Symbol.Symbol.symbol;
import static Symbol.Table.levelfull;
import static java.lang.System.exit;

public class AssignmentExpression extends Expression {
    public Expression lhs, rhs;

    public AssignmentExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = lhs.ty;
    }

    public void ASTprint() {
        //System.out.println("Assign");
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
        //System.out.println("test levelfull = " + levelfull);
        //System.out.println(lhs.ty.getClass().getSimpleName());
        //System.out.println(rhs.ty.getClass().getSimpleName());
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.leftv == 0) {
                //System.out.println("assign: not left value");
                throw new RuntimeException("");
            } else {
                //System.out.println("assign: Type Agree");
                if (rhs.ty instanceof NullType) {
                    ty = lhs.ty;
                } else {
                    ty = rhs.ty;
                }
            }
        } else {
            //System.out.println("assign:Type Not Agree");
            //System.out.println(lhs.getClass().getSimpleName());
            //System.out.println(rhs.getClass().getSimpleName());
            throw new RuntimeException("");
        }
    }
    public String getName() {
        return name;
    }
    /*
    public Address IRL() throws Exception {
        Address src1, src2;

        if (lhs instanceof PostStructExpression) {
            src1 = ((PostStructExpression)lhs).IRL();
        } else if (lhs instanceof PostArrayExpression) {
            src1 = ((PostArrayExpression)lhs).IRL();
        } else {
            src1 = lhs.IRE();
        }

        src2 = rhs.IRE();

        /*
        if (src2 instanceof Memory) { //impossible
            Address src3 = new Temp();
            loc.add(new MemoryLoad((Memory)src2, src3));
            src2 = src3;
        }

        if (src2 instanceof CallExpr && src1 instanceof Memory) {
            Address src3 = new Temp();
            loc.add(new CallStmt((CallExpr)src2, src3));
            src2 = src3;
        } else if (src2 instanceof CallExpr){
            loc.add(new CallStmt((CallExpr)src2, src1));
            return src1;
        }

        if (src1 instanceof Memory) {
            Address sc = ((Memory)src1).src;
            src1 = new Memory(sc, ((Memory) src1).offset);
            loc.add(new MemoryStore((Memory)src1, src2));
        } else {
            loc.add(new Assign(src1, src2));
        }

        return src1;
    }
    */

    public Address IRE() throws Exception {
        Address src1, src2;

        if (lhs instanceof PostStructExpression) {
            src1 = ((PostStructExpression)lhs).IRL();
        } else if (lhs instanceof PostArrayExpression) {
            src1 = ((PostArrayExpression)lhs).IRL();
        } else {
            src1 = lhs.IRE();
        }

        src2 = rhs.IRE();

        /*
        if (src2 instanceof Memory) { //impossible
            Address src3 = new Temp();
            loc.add(new MemoryLoad((Memory)src2, src3));
            src2 = src3;
        }
        */
        /*
        if (src2 instanceof CallExpr && src1 instanceof Memory) {
            Address src3 = new Temp();
            loc.add(new CallStmt((CallExpr)src2, src3));
            src2 = src3;
        } else if (src2 instanceof CallExpr){
            loc.add(new CallStmt((CallExpr)src2, src1));
            return src1;
        }
        */

        if (src1 instanceof Memory) {
            Address sc = ((Memory)src1).src;
            src1 = new Memory(sc, ((Memory) src1).offset);
            if (src2 instanceof IntegerConst) {
                Temp si = new Temp();
                loc.add(new Assign(si, src2));
                src2 = si;
            }
            loc.add(new MemoryStore((Memory)src1, src2));
            //return src2;
        } else {
            loc.add(new Assign(src1, src2));
        }

        return src2;
    }
}

