package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.Type;
import AST.Statement.Type.VoidType;
import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.Mop;
import static Parser.ClassNameListener.loc;
import static Symbol.Symbol.symbol;

public class PostArgumentExpression extends Expression {
    public Expression uhs;

    public PostArgumentExpression(Expression uhs) {
        this.uhs = uhs;
    }

    public void ASTprint() {
        //System.out.println("PostArguExpr");
        uhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        uhs.Final();
        name = uhs.getName();
        //System.out.println(name);
        if (uhs instanceof PostStructExpression) {
            if (name.equals("length")) {
                //System.out.println("postArgu: Strfunc has no para, ok");
                ty = uhs.ty;
            } else if (name.equals("parseInt")) {
                //System.out.println("postArgu: Strfunc has no para, ok");
                ty = uhs.ty;
            } else if(name.equals("size")) {
                //System.out.println("postArgu: Strfunc has no para, ok");
                ty = uhs.ty;
            } else {
                //System.out.println("postArgu: not a Strfunc");
                throw new RuntimeException("");
            }
        } else {
            if (Mop.get(symbol(uhs.getName())) instanceof FuncExpression) {
                FuncExpression x = (FuncExpression) Mop.get(symbol(uhs.getName()));

                if (x.para == null) {
                    //System.out.println("postArgu: func has no para, ok");
                    ty = uhs.ty;
                } else {
                    //System.out.println("postArgu: func should no para");
                    throw new RuntimeException("");
                }
            } else {
                //System.out.println("postArgu: not a func");
                throw new RuntimeException("");
            }
        }
    }

    public Address IRE() throws Exception {
        //uhs.Final();
        name = uhs.getName();
        ////System.out.println(name);
        Address De = null;
        if (uhs instanceof PostStructExpression) {
            if (name.equals("size")) {
                return uhs.IRE();
            } else if (name.equals("length")) {
                name = "func_string.length";
                Address src1 = ((PostStructExpression)uhs).lhs.IRE();
                De = new CallExpr(name, src1);
                Temp src = new Temp();
                loc.add(new CallStmt((CallExpr)De, src));
                return src;
            } else if (name.equals("parseInt")) {
                name = "func_string.parseInt";
                Address src1 = ((PostStructExpression)uhs).lhs.IRE();
                De = new CallExpr(name, src1);
                Temp src = new Temp();
                loc.add(new CallStmt((CallExpr)De, src));
                return src;
            }
            /*
            if (name.equals("length")) {
                //System.out.println("postArgu: Strfunc has no para, ok");
                ty = uhs.ty;
            } else if (name.equals("parseInt")) {
                //System.out.println("postArgu: Strfunc has no para, ok");
                ty = uhs.ty;
            } else {
                //System.out.println("postArgu: not a Strfunc");
                throw new RuntimeException("");
            }*/
        } else {
            if (Mop.get(symbol(name)) instanceof FuncExpression) {
                Type TT = ((FuncExpression)Mop.get(symbol(name))).ty;
                if (name.equals("getInt")) {
                    name = "func_getInt";
                } else if (name.equals("getString")) {
                    name = "func_getString";
                } else {
                    name = name + 44;
                }

                De = new CallExpr(name);
                if (TT instanceof VoidType) {

                } else {
                    Temp src = new Temp();
                    loc.add(new CallStmt((CallExpr)De, src));
                    return src;
                }
            } else {
                //System.out.println("postArgu: not a func");
                throw new RuntimeException("");
            }
        }
        return De;
    }
}
