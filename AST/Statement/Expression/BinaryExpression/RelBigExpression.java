package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import AST.Statement.Type.IntType;
import AST.Statement.Type.StringType;
import IR.*;

import java.util.ArrayList;

import static IR.RelationalOp.sge;
import static IR.RelationalOp.sgt;
import static Parser.ClassNameListener.loc;

public class RelBigExpression extends Expression {
    public Expression lhs, rhs;

    public RelBigExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new BoolType();
    }

    public void ASTprint() {
        //System.out.println("BigThan");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof IntType) {
                ty = new BoolType();
            } else if (lhs.ty instanceof StringType) {
                ty = new BoolType();
            } else {
                //System.out.println("big: un def");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("big: Type Not Agree");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        if (lhs.ty instanceof StringType) {
            Address args = new MultiT(rhs.IRE(), lhs.IRE());
            Temp De = new Temp();
            CallStmt Ge = new CallStmt(new CallExpr("func_stringLess", args), De);
            loc.add(Ge);
            return De;
        } else {
            Temp De = new Temp();
            RelationalExpr Ge = new RelationalExpr(De, lhs.IRE(), sgt, rhs.IRE());
            loc.add(Ge);
            ////System.out.println("Reach IRE ADD");
            return De;
        }
    }
}
