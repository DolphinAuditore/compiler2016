package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import AST.Statement.Type.MultiType;
import IR.Address;
import IR.MultiT;
import IR.Quadruple;

import java.util.ArrayList;

public class ArgumentListDef extends Expression {
    public Expression lhs, rhs;

    public ArgumentListDef(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;

    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public void Pre2() {}
    public void Final() {
        lhs.Final();
        rhs.Final();
        ty = new MultiType(rhs.ty, (MultiType)lhs.ty);
    }
    public String getName() {
        return name;
    }

    public Address IRE() throws Exception {
        Address De = new MultiT(lhs.IRE(), rhs.IRE());
        return De;
    }
}
