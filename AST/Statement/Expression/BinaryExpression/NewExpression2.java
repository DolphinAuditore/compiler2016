package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.DefType;
import AST.Statement.Type.Type;
import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.loc;

public class NewExpression2 extends Expression {
    public Type lhs;

    public NewExpression2(Type lhs) {
        this.lhs = lhs;
    }

    public void ASTprint() {
        //System.out.println("New");
        lhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Pre2() {
        this.lhs = lhs;
        ty = lhs;
    }
    public void Final() {
        lhs.Final();
        this.lhs = lhs;
        ty = lhs;
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        Alloc Ge;
        if (lhs instanceof DefType) {
            Ge = new Alloc(De, ((DefType)lhs).size);
        } else {
            Ge = new Alloc(De, 4);
        }
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
    }
}
