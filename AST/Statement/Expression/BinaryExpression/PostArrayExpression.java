package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Expression.UnaryExpression.ID;
import AST.Statement.Type.ArrayType;
import AST.Statement.Type.IntType;
import IR.*;
import Symbol.Symbol;

import java.util.ArrayList;

import static IR.ArithmeticOp.add;
import static IR.ArithmeticOp.mul;
import static Parser.ClassNameListener.Mop;
import static Parser.ClassNameListener.loc;
import static Symbol.Symbol.symbol;

public class PostArrayExpression extends Expression {
    public Expression lhs, rhs;

    public PostArrayExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }
    public String getName() {
        return name;
    }
    public void Final() {
        lhs.Final();
        rhs.Final();
        if (rhs.ty instanceof IntType) {
            //ok
        } else {
            //System.out.println("Array Xiabiao bu shi int");
        }
        if (lhs.ty instanceof ArrayType) {
            ArrayType ty1 = new ArrayType();
            ty1.lhs = ((ArrayType) lhs.ty).lhs;
            ty1.rhs = ((ArrayType) lhs.ty).rhs;
            //System.out.println(ty1.lhs.getClass().getSimpleName());
            //System.out.println(ty1.rhs);
            ty1.rhs--;
            if (ty1.rhs == 0) {
                ty = ty1.lhs;
            } else {
                ty = ty1;
            }
            leftv = lhs.leftv;
        }
    }

    public Address IRL() throws Exception {

        Address src = lhs.IRE();
        Address offset = rhs.IRE();

        Temp Nsize = new Temp();
        ArithmeticExpr Fe = new ArithmeticExpr(Nsize, new IntegerConst(4), mul, offset);
        loc.add(Fe);

        Fe = new ArithmeticExpr(Nsize, Nsize, add, new IntegerConst(4));
        loc.add(Fe);

        if (src instanceof Memory) {
            Temp src2 = new Temp();
            loc.add(new MemoryLoad((Memory)src, src2));
            src = src2;
        }

        Temp srcoffset = new Temp();
        Fe = new ArithmeticExpr(srcoffset, src, add, Nsize);
        loc.add(Fe);

        Memory De = new Memory(srcoffset, 0);
        return De;
    }

    public Address IRE() throws Exception {

        Address src = lhs.IRE();
        Address offset = rhs.IRE();

        Temp Nsize = new Temp();
        ArithmeticExpr Fe = new ArithmeticExpr(Nsize, new IntegerConst(4), mul, offset);
        loc.add(Fe);

        Fe = new ArithmeticExpr(Nsize, Nsize, add, new IntegerConst(4));
        loc.add(Fe);

        if (src instanceof Memory) {
            Temp src2 = new Temp();
            loc.add(new MemoryLoad((Memory)src, src2));
            src = src2;
        }

        Temp srcoffset = new Temp();
        Fe = new ArithmeticExpr(srcoffset, src, add, Nsize);
        loc.add(Fe);

        Memory De = new Memory(srcoffset, 0);

        Temp srcx = new Temp();
        loc.add(new MemoryLoad(De, srcx));
        return srcx;
    }
}
