package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.ArrayType;
import AST.Statement.Type.IntType;
import AST.Statement.Type.Type;
import AST.Statement.Type.UnType;
import IR.*;

import java.util.ArrayList;

import static IR.ArithmeticOp.add;
import static IR.ArithmeticOp.mul;
import static Parser.ClassNameListener.loc;
import static java.lang.System.exit;

public class NewExpression extends Expression {
    public Type lhs;
    public Expression rhs;

    public NewExpression(Type lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        //System.out.println("New");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Pre2() {
        rhs.Pre2();
        ArrayType ty1 = (ArrayType)rhs.ty;
        ty1.lhs = lhs;
        ty = ty1;
    }
    public void Final() {
        lhs.Final();
        rhs.Final();
        ArrayType ty1 = (ArrayType)rhs.ty;
        ty1.lhs = lhs;
        ty = ty1;
    }
    public Address IRE() throws Exception {
        Temp Nsize = new Temp();
        Address Isize = rhs.IRE();
        ArithmeticExpr Fe = new ArithmeticExpr(Nsize, new IntegerConst(4), mul, Isize);
        loc.add(Fe);

        Fe = new ArithmeticExpr(Nsize, Nsize, add, new IntegerConst(4));
        loc.add(Fe);

        Temp De = new Temp();
        Alloc Ge = new Alloc(De, Nsize);
        loc.add(Ge);

        loc.add(new MemoryStore(new Memory(De, 0), Isize));

        return De;
    }
}
