package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import AST.Statement.Type.MultiType;
import IR.Address;
import IR.FuncPart;
import IR.MultiT;
import IR.Quadruple;

import java.util.ArrayList;

public class DeclFunc3 extends Expression {
    public Expression lhs, rhs;

    public DeclFunc3(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Pre2() {
        rhs.Pre2();
        MultiType ty1 = (MultiType)rhs.ty;
        ty = ty1;
    }

    public void Final() {
        rhs.Final();
    }

    public Address IRE() throws Exception {
        name = lhs.getName();

        if (name.equals("print")) {
            name = "func_print";
        } else if (name.equals("println")) {
            name = "func_println";
        } else if (name.equals("toString")) {
            name = "func_toString";
        } else {
            name = name + 44;
        }

        Address De = new FuncPart(lhs.getName(), rhs.IRE());
        return De;
    }
}
