package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.ArrayType;
import AST.Statement.Type.IntType;
import AST.Statement.Type.Type;
import IR.Address;
import IR.Quadruple;

import java.util.ArrayList;

import static java.lang.System.exit;

public class DimExpression2 extends Expression {
    public Expression uhs;

    public DimExpression2(Expression uhs) {
        this.uhs = uhs;
        //ty = new ArrayType(uhs);
    }
    public String getName() {
        return name;
    }

    public void Pre2() {
        uhs.Pre2();
        ArrayType ty1 = (ArrayType)uhs.ty;
        ty1.rhs++;
        ty = ty1;
    }

    public void Final() {
        uhs.Final();
        ArrayType ty1 = (ArrayType)uhs.ty;
        ty1.rhs++;
        ty = ty1;
    }

    public Address IRE() throws Exception {
        return uhs.IRE();
    }
}
