package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import AST.Statement.Type.IntType;
import AST.Statement.Type.StringType;
import IR.*;

import java.util.ArrayList;

import static IR.ArithmeticOp.add;
import static Parser.ClassNameListener.loc;

public class AddIncExpression extends Expression {
    public Expression lhs, rhs;

    public AddIncExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new BoolType();
    }

    public void ASTprint() {
        //System.out.println("Add");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof IntType) {
                ty = lhs.ty;
            } else if (lhs.ty instanceof StringType) {
                ty = lhs.ty;
            } else {
                //System.out.println("addinc: can't add it");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("addinc: Type Not Agree");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        if (lhs.ty instanceof StringType) {
            Address args = new MultiT(lhs.IRE(), rhs.IRE());
            Temp De = new Temp();
            CallStmt Ge = new CallStmt(new CallExpr("func_stringConcatenate", args), De);
            loc.add(Ge);
            return De;
        } else {
            Temp De = new Temp();
            ArithmeticExpr Ge = new ArithmeticExpr(De, lhs.IRE(), add, rhs.IRE());
            loc.add(Ge);
            ////System.out.println("Reach IRE ADD");
            return De;
        }
    }
}
