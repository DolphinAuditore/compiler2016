package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;
import IR.Address;
import IR.FuncPart;
import IR.MultiT;
import IR.Quadruple;

import java.util.ArrayList;

public class DeclFunc extends Expression {
    public Expression uhs;

    public DeclFunc(Expression uhs) {
        this.uhs = uhs;
    }

    public void ASTprint() {
        uhs.ASTprint();
    }

    public String getName() {
        return name;
    }

    public Address IRE() throws Exception {
        name = uhs.getName();
        if (name.equals("getInt")) {
            name = "func_getInt";
        } else if (name.equals("getString")) {
            name = "func_getString";
        } else {
            name = name + 44;
        }
        /*
        if (name.equals("print")) {
            name = "func_print";
        }
        if (name.equals("println")) {
            name = "func_println";
        }
        */

        Address De = new FuncPart(uhs.getName());
        return De;
    }
}
