package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import IR.*;

import java.util.ArrayList;

import static IR.LogicalOp.and;
import static IR.LogicalOp.or;
import static Parser.ClassNameListener.loc;

public class LogOrExpression extends Expression {
    public Expression lhs, rhs;

    public LogOrExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new BoolType();
    }

    public void ASTprint() {
        //System.out.println("LogOr");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }
    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof BoolType) {
                ty = lhs.ty;
            } else {
                //System.out.println("logor: un def");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("logor: Type Not Agree");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        Assign Ae = new Assign(De, lhs.IRE());
        loc.add(Ae);

        Label lab1 = new Label();
        Label lab2 = new Label();
        JumpEnd end1 = new JumpEnd(lab1);
        JumpEnd end2 = new JumpEnd(lab2);
        Jump to1 = new Jump(lab1);
        Jump to2 = new Jump(lab2);

        Branch Be = new Branch(De, lab2, lab1);
        loc.add(Be);

        loc.add(end1);
        LogicalExpr Ge = new LogicalExpr(De, De, or, rhs.IRE());
        loc.add(Ge);
        loc.add(to2);

        loc.add(end2);
        return De;
        ////System.out.println("Reach IRE ADD");

    }
}
