package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.ArrayType;
import AST.Statement.Type.IntType;
import AST.Statement.Type.Type;
import AST.Statement.Type.UnType;
import IR.Address;
import IR.Quadruple;

import java.util.ArrayList;

import static java.lang.System.exit;

public class DimExpression extends Expression {
    public Expression uhs;

    public DimExpression(Expression uhs) {
        this.uhs = uhs;
        //ty = new ArrayType(uhs);
    }
    public String getName() {
        return name;
    }

    public void Pre2() {
        uhs.Pre2();
        if (uhs.ty instanceof IntType) {
            ArrayType ty1 = new ArrayType();
            ty1.lhs = new UnType();
            ty1.rhs = 1;
            ty = ty1;
        } else {
            //System.out.println("Array Not Int");
            throw new RuntimeException("");
        }
    }

    public void Final() {
        uhs.Final();
        if (uhs.ty instanceof IntType) {
            ArrayType ty1 = new ArrayType();
            ty1.lhs = new UnType();
            ty1.rhs = 1;
            ty = ty1;
        } else {
            //System.out.println("Array Not Int");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        return uhs.IRE();
    }
}
