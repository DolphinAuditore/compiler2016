package AST.Statement.Expression.BinaryExpression;

import AST.DeclarationFull.ClassDeclaration;
import AST.Statement.Expression.Expression;
import AST.Statement.Type.*;
import IR.Address;
import IR.MultiT;
import IR.Quadruple;
import IR.Temp;

import java.util.ArrayList;

import static AST.Statement.DeclarationStatement.Declaration.hsx;
import static Parser.ClassNameListener.Mop;
import static Symbol.Symbol.symbol;
import static Symbol.Table.levelfull;
import static java.lang.System.exit;

public class ParaListDef extends Expression {
    public Expression lhs, rhs;
    public MultiType para;

    public ParaListDef(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }

    public String getName() {
        return name;
    }

    public void Pre2() {
        ////System.out.println("WTF?!!!");
        //System.out.println("ParaL");
        //System.out.println(rhs.getClass().getSimpleName());
        lhs.Pre2();
        para = new MultiType();
        para.lhs = ((ParaDecl)rhs).lhs;
        //System.out.println("*** : " + para.lhs.getClass().getSimpleName());
        para.rhs = (MultiType)lhs.ty;
        ty = para;
    }
    public void Final() {
        //System.out.println("We in paralist");
        lhs.Final();
        rhs.Final();
    }

    public Address IRE() throws Exception {
        Address De = new MultiT(lhs.IRE(), rhs.IRE());
        return De;
    }

}
