package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.MultiType;
import AST.Statement.Type.Type;
import IR.Quadruple;

import java.util.ArrayList;

public class FuncExpression extends Expression {
    public String name;
    public MultiType para;

    public FuncExpression(Type hs1, String name, MultiType para) {
        ty = hs1;
        this.name = name;
        this.para = para;
    }

    public void ASTprint() {
        //System.out.println("func" + name);
    }

    public String getName() {
        return name;
    }

}
