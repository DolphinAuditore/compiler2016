package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Statement;

public class DeclFunc2 extends Expression {
    public Expression lhs, rhs;

    public DeclFunc2(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }
}
