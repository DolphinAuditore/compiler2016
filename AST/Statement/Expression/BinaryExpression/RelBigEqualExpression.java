package AST.Statement.Expression.BinaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import AST.Statement.Type.IntType;
import AST.Statement.Type.StringType;
import IR.*;

import java.util.ArrayList;

import static IR.RelationalOp.sge;
import static Parser.ClassNameListener.loc;

public class RelBigEqualExpression extends Expression {
    public Expression lhs, rhs;

    public RelBigEqualExpression(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
        ty = new BoolType();
    }

    public void ASTprint() {
        //System.out.println("BigEqual");
        lhs.ASTprint();
        rhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
        if (lhs.ty.typeEq(rhs.ty) == 1) {
            if (lhs.ty instanceof IntType) {
                ty = new BoolType();
            } else if (lhs.ty instanceof StringType) {
                ty = new BoolType();
            } else {
                //System.out.println("bige: un def");
                throw new RuntimeException("");
            }
        } else {
            //System.out.println("bige: Type Not Agree");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        RelationalExpr Ge = new RelationalExpr(De, lhs.IRE(), sge, rhs.IRE());
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
    }
}
