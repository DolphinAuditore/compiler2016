package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;

public class UnaryNewExpression extends Expression {

    //Not used

    public Expression uhs;

    public UnaryNewExpression(Expression uhs) {
        this.uhs = uhs;
        ty = uhs.ty;
    }

    public void ASTprint() {
        uhs.ASTprint();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        uhs.Final();
        ty = uhs.ty;
    }

}
