package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;
import IR.Address;
import IR.ArithmeticExpr;
import IR.Quadruple;
import IR.Temp;

import java.util.ArrayList;

import static IR.ArithmeticOp.tilde;

public class UnaryPNewExpression extends Expression {
    public Expression uhs;

    public UnaryPNewExpression(Expression uhs) {
        this.uhs = uhs;
        ty = new IntType();
    }
    public String getName() {
        return name;
    }
    public void Final() {
        uhs.Final();
        if (uhs.ty instanceof IntType) {
            ty = uhs.ty;
        } else {
            //System.out.println("UnP: not a int");
            throw new RuntimeException("");
        }
    }
    public Address IRE() throws Exception {
        //Temp De = new Temp();
        //ArithmeticExpr Ge = new ArithmeticExpr(De, tilde, uhs.IRE(loc));
        //loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return uhs.IRE();
    }
}
