package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.BinaryExpression.PostArrayExpression;
import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;
import IR.*;

import java.util.ArrayList;

import static IR.ArithmeticOp.add;
import static IR.ArithmeticOp.tilde;
import static Parser.ClassNameListener.loc;

public class UnaryPPExpression extends Expression {
    public Expression uhs;

    public UnaryPPExpression(Expression uhs) {
        this.uhs = uhs;
        ty = new IntType();
    }
    public String getName() {
        return name;
    }
    public void Final() {
        uhs.Final();
        if (uhs.ty instanceof IntType) {
            ty = uhs.ty;
        } else {
            //System.out.println("UnPP: not a int");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Address src;
        //src = uhs.IRE();
        if (uhs instanceof PostArrayExpression) {
            src = ((PostArrayExpression) uhs).IRL();
        } else {
            src = uhs.IRE();
        }
        if (src instanceof Memory) {
            //System.out.println("RT here");
            Address src2 = new Temp();
            loc.add(new MemoryLoad((Memory)src, src2));
            loc.add(new ArithmeticExpr(src2, src2, add, new IntegerConst(1)));
            loc.add(new MemoryStore((Memory)src, src2));
            return src2;
        } else {
            loc.add(new ArithmeticExpr(src, src, add, new IntegerConst(1)));
            return src;
        }

        ////System.out.println("Reach IRE ADD");
        /*
        似乎有数组assignexpr的难度（废话，a = a + 1不就是assignexpr...）
         */
    }

    /*
    public Address IRE(ArrayList<Quadruple> loc) throws Exception {
        Address src1, src2;
        src1 = lhs.IRE(loc);
        src2 = rhs.IRE(loc);

        if (src2 instanceof Memory) {
            Address src3 = new Temp();
            loc.add(new MemoryLoad((Memory)src2, src3));
            src2 = src3;
        }

        if (src1 instanceof Memory) {
            loc.add(new MemoryStore((Memory)src1, src2));
        } else {
            loc.add(new Assign(src1, src2));
        }
        return src1;
    }
     */
}
