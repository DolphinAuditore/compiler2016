package AST.Statement.Expression.UnaryExpression;

import AST.DeclarationFull.ClassDeclaration;
import AST.DeclarationFull.FuncDeclaration;
import AST.Statement.Expression.BinaryExpression.FuncExpression;
import AST.Statement.Expression.Expression;
import AST.Statement.Type.UnType;
import IR.*;
import Symbol.Symbol;

import java.util.ArrayList;

import static AST.Statement.DeclarationStatement.Declaration.hsx;
import static IR.ArithmeticOp.add;
import static Parser.ClassNameListener.Mop;
import static Symbol.Symbol.symbol;
import static Symbol.Table.levelfull;
import static java.lang.System.exit;

public class ID extends Expression {

    public String name;

    public ID(String name) {
        this.name = name;
        ty = new UnType();
        //System.out.println("ID name: " + name);
        /*
        if (name.equals("key")) {
            //System.out.println("We have Key!!");
        }
        */
        ////System.out.println("name:"+this.name);
        ////System.out.println("type:"+ty.getClass().getSimpleName());
    }

    public void ASTprint() {
        //System.out.println(name);
    }

    public void Pre2() {

    }

    public void Final() {
        if (Mop.get(symbol(name)) == null) {
            if (hsx instanceof UnType) {
                //System.out.println("can't use undef ID");
                throw new RuntimeException("");
            } else {
                ty = hsx;
                leftv = 1;
                Mop.put(symbol(name), this);
            }
        } else if (Mop.get(symbol(name)) instanceof ClassDeclaration) {

            /*//System.out.println("can't use type as ID");
            throw new RuntimeException("");*/
        } else {
            if (hsx instanceof UnType) {
                ty = ((Expression) Mop.get(symbol(name))).ty;
                //System.out.println("ID find: " + name + ", Type: " + ty.getClass().getSimpleName());
                if (Mop.get(symbol(name)) instanceof FuncExpression) {

                } else {
                    leftv = 1;
                }
                /*
                if (ty instanceof FuncExpression || ty instanceof ClassDeclaration) {

                }*/
            } else {
                if (Mop.getlevel(symbol(name)) == levelfull) {
                    //System.out.println("ID redef");
                    throw new RuntimeException("");
                } else {
                    ty = hsx;
                    leftv = 1;
                    Mop.put(symbol(name), this);
                }
            }
        }
    }
    public String getName() {
        return name;
    }

    public Address IRE() throws Exception {

        ////System.out.println("IRE test levelfull = " + levelfull);
        ////System.out.println("IRE test name = " + name);
        if (Mop.get(symbol(name)) == null) {
            if (hsx instanceof UnType) {
                //System.out.println("can't use undef ID");
                throw new RuntimeException("");
            } else {
                ty = hsx;
                leftv = 1;
                Mop.put(symbol(name), this);
            }
        } else if (Mop.get(symbol(name)) instanceof ClassDeclaration) {

            /*//System.out.println("can't use type as ID");
            throw new RuntimeException("");*/
        } else {
            if (hsx instanceof UnType) {
                ty = ((Expression) Mop.get(symbol(name))).ty;
                ////System.out.println("ID find: " + name + ", Type: " + ty.getClass().getSimpleName());
                if (Mop.get(symbol(name)) instanceof FuncExpression) {

                } else {
                    leftv = 1;
                }
                /*
                if (ty instanceof FuncExpression || ty instanceof ClassDeclaration) {

                }*/
            } else {
                if (Mop.getlevel(symbol(name)) == levelfull) {
                    //System.out.println("ID redef");
                    //System.out.println("levelfull = " + levelfull);
                    throw new RuntimeException("");
                } else {
                    ty = hsx;
                    leftv = 1;
                    Mop.put(symbol(name), this);
                }
            }
        }

        Temp De = new Temp(Mop.getnum(symbol(name)));
        return De;

        /*
        Temp De = new Temp();
        ArithmeticExpr Ge = new ArithmeticExpr(De, lhs.IRE(loc), add, rhs.IRE(loc));
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
        */
    }
}
