package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import AST.Statement.Type.IntType;
import IR.*;

import java.util.ArrayList;


import static IR.ArithmeticOp.sub;
import static Parser.ClassNameListener.loc;

public class UnaryNNewExpression extends Expression {
    public Expression uhs;

    public UnaryNNewExpression(Expression uhs) {
        ty = new BoolType();
        this.uhs = uhs;
    }
    public String getName() {
        return name;
    }
    public void Final() {
        uhs.Final();
        if (uhs.ty instanceof BoolType) {
            ty = uhs.ty;
        } else {
            //System.out.println("UnN: not a Bool");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        //LogicalExpr Ge = new LogicalExpr(De, LogicalOp.not, uhs.IRE());
        ArithmeticExpr Ge = new ArithmeticExpr(De, new IntegerConst(1), sub, uhs.IRE());
        loc.add(Ge);
        ////System.out.println("Reach IRE LogNot");
        return De;
    }
}
