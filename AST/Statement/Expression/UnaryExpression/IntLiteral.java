package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;
import IR.*;

import java.util.ArrayList;

import static IR.ArithmeticOp.add;

public class IntLiteral extends Expression {

    public String name;

    public IntLiteral(String name) {
        this.name = name;
        ty = new IntType();
    }

    public void ASTprint() {
        //System.out.println(name);
    }
    public String getName() {
        return name;
    }

    public Address IRE() throws Exception {
        IntegerConst De = new IntegerConst(Integer.parseInt(name));
        ////System.out.println("Reach IntLiteral");
        return De;
    }
}
