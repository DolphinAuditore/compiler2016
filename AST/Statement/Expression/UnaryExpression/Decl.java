package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.UnType;

public class Decl extends Expression {

    Expression uhs;

    public Decl(Expression uhs) {
        this.uhs = uhs;
        ty = uhs.ty;
    }

    public void ASTprint() {
        uhs.ASTprint();
    }
    public String getName() {
        return name;
    }
}
