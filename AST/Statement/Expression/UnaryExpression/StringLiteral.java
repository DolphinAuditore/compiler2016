package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.StringType;
import IR.Address;
import IR.IntegerConst;
import IR.StringConst;

import static Parser.ClassNameListener.ST;
import static Parser.ClassNameListener.SaintTerror;
import static Symbol.Symbol.symbol;

public class StringLiteral extends Expression {

    public String name;

    public StringLiteral(String name) {
        this.name = name;
        ty = new StringType();
    }

    public void ASTprint() {
        //System.out.println(name);
    }
    public String getName() {
        return name;
    }

    public Address IRE() throws Exception {

        /*
        String str = "你叫神马？我叫Tesson.";
        for (int i = 0; i < str.length(); i++) {
            char  item =  str.charAt(i);
            System.out.println(item);
        }
        */

        StringConst De = new StringConst(name, ST);
        SaintTerror.add(name);
        ST++;
        return De;
    }
}
