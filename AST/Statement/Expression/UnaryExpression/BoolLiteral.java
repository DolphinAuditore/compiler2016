package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.BoolType;
import AST.Statement.Type.IntType;
import IR.Address;
import IR.BooleanConst;
import IR.IntegerConst;
import IR.Quadruple;

import java.util.ArrayList;

public class BoolLiteral extends Expression {

    public String name;

    public BoolLiteral(String name) {
        this.name = name;
        ty = new BoolType();
        //if (name.equals("true")) {}
    }

    public void ASTprint() {
        //System.out.println(name);
    }
    public String getName() {
        return name;
    }
    public String toString() { return name; }

    public Address IRE() throws Exception {
        BooleanConst De = new BooleanConst(name.equals("true"));
        ////System.out.println("Reach IntLiteral");
        return De;
    }
}
