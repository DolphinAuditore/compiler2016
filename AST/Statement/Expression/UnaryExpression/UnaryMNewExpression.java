package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;
import IR.*;

import java.util.ArrayList;

import static IR.ArithmeticOp.neg;
import static IR.RelationalOp.sge;
import static Parser.ClassNameListener.loc;

public class UnaryMNewExpression extends Expression {
    public Expression uhs;

    public UnaryMNewExpression(Expression uhs) {
        ty = new IntType();
        this.uhs = uhs;
    }
    public String getName() {
        return name;
    }
    public void Final() {
        uhs.Final();
        if (uhs.ty instanceof IntType) {
            ty = uhs.ty;
        } else {
            //System.out.println("UnM: not a int");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Temp De = new Temp();
        ArithmeticExpr Ge = new ArithmeticExpr(De, neg, uhs.IRE());
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
    }
}
