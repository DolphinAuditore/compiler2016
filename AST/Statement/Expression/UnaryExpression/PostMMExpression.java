package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.BinaryExpression.PostArrayExpression;
import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;
import IR.*;

import java.util.ArrayList;

import static IR.ArithmeticOp.add;
import static IR.ArithmeticOp.sub;
import static Parser.ClassNameListener.loc;

public class PostMMExpression extends Expression {
    public Expression uhs;

    public PostMMExpression(Expression uhs) {
        this.uhs = uhs;
        ty = new IntType();
    }
    public String getName() {
        return name;
    }

    public void Final() {
        uhs.Final();
        if (uhs.ty instanceof IntType) {
            ty = uhs.ty;
        } else {
            //System.out.println("PostMM: not a int");
            throw new RuntimeException("");
        }
    }

    public Address IRE() throws Exception {
        Address De = new Temp();
        Address src;
        //src = uhs.IRE();
        if (uhs instanceof PostArrayExpression) {
            src = ((PostArrayExpression) uhs).IRL();
        } else {
            src = uhs.IRE();
        }
        if (src instanceof Memory) {
            loc.add(new MemoryLoad((Memory)src, De));
            Address src2 = new Temp();
            loc.add(new MemoryLoad((Memory)src, src2));
            loc.add(new ArithmeticExpr(src2, src2, sub, new IntegerConst(1)));
            loc.add(new MemoryStore((Memory)src, src2));
            return De;
        } else {
            loc.add(new Assign(De, src));
            loc.add(new ArithmeticExpr(src, src, sub, new IntegerConst(1)));
            return De;
        }

        ////System.out.println("Reach IRE ADD");
    }
}
