package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.NullType;
import IR.Address;
import IR.IntegerConst;

public class NULL extends Expression {

    public NULL() {
        ty = new NullType();
    }

    public void ASTprint() {

    }
    public String getName() {
        return name;
    }

    public Address IRE() throws Exception {
        IntegerConst De = new IntegerConst(0);
        ////System.out.println("Reach IntLiteral");
        return De;
    }
}
