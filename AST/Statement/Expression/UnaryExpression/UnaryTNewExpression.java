package AST.Statement.Expression.UnaryExpression;

import AST.Statement.Expression.Expression;
import AST.Statement.Type.IntType;
import IR.*;

import java.util.ArrayList;

import static IR.ArithmeticOp.tilde;
import static IR.RelationalOp.sge;
import static Parser.ClassNameListener.loc;

public class UnaryTNewExpression extends Expression {
    public Expression uhs;

    public UnaryTNewExpression(Expression uhs) {
        this.uhs = uhs;
        ty = new IntType();
    }
    public String getName() {
        return name;
    }
    public void Final() {
        uhs.Final();
        if (uhs.ty instanceof IntType) {
            ty = uhs.ty;
        } else {
            //System.out.println("UnT: not a int");
            throw new RuntimeException("");
        }
    }
    public Address IRE() throws Exception {
        Temp De = new Temp();
        ArithmeticExpr Ge = new ArithmeticExpr(De, tilde, uhs.IRE());
        loc.add(Ge);
        ////System.out.println("Reach IRE ADD");
        return De;
    }
}
