package AST.Statement.Expression.VarExpression;

import AST.Statement.Type.Type;

public class ArrayVar extends VarExpression {

    public String name;
    public Type lhs;
    public int  rhs;

    public ArrayVar(String name) {
        this.name = name;
    }

    public void ASTprint() {
        //System.out.println("ArrayVar:"+name);
    }

    public void Pre2() {

    }

    public void Final() {

    }
}
