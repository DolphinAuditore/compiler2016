package AST.Statement.Expression;

import AST.Statement.Statement;
import AST.Statement.Type.Type;
import IR.Address;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public abstract class Expression implements Statement {
    public Type ty;
    public String name;
    public int leftv;
    public void ASTprint(){};
    public void Pre2(){};
    public void Final(){};
    public void IR() throws Exception{};
    public Address IRE() throws Exception {
        Address De = null;
        return De;
    }
    public abstract String getName();

    public void IRB(TA ThreeAddress) throws Exception {

    }
}

