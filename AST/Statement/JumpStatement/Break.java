package AST.Statement.JumpStatement;

import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.iterend;
import static Parser.ClassNameListener.iterlevel;
import static Parser.ClassNameListener.loc;

public class Break extends JumpStatement {

    public Break() {

    }

    public String name;

    public Break(String name) {
        this.name = name;
    }

    public void ASTprint() {
        //System.out.println(name);
    }

    public void Final() {
        if (iterlevel == 0) {
            //System.out.println("But no iter here");
            throw new RuntimeException("");
        } else {

        }
    }

    public void IR() throws Exception {
        ////System.out.println("it2 " + iterlevel);
        Jump to = new Jump(iterend.get(iterlevel - 1));
        loc.add(to);
    }
}
