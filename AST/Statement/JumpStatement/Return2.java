package AST.Statement.JumpStatement;

import AST.Statement.Expression.Expression;
import IR.*;

import java.util.ArrayList;

import static Parser.ClassNameListener.loc;

public class Return2 extends JumpStatement {

    public Expression uhs;

    public Return2(Expression uhs) {
        this.uhs = uhs;
    }

    public void ASTprint() {
        //System.out.println("Return");
        uhs.ASTprint();
    }

    public void Final() {
        uhs.Final();
    }

    public void IR() throws Exception {
        Address src;
        src = uhs.IRE();
        /*
        if (src instanceof Memory) {
            System.out.println("ASASDSDASD");
        }
        */
        ReturnIR to = new ReturnIR(src);
        loc.add(to);
    }
}
