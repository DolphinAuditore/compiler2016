package AST.Statement.JumpStatement;

import IR.ReturnIR;
import IR.Quadruple;

import java.util.ArrayList;

import static Parser.ClassNameListener.loc;


public class Return extends JumpStatement {

    public Return() {

    }

    public String name;

    public Return(String name) {
        this.name = name;
    }

    public void ASTprint() {
        //System.out.println(name);
    }

    public void IR() throws Exception {
        ReturnIR to = new ReturnIR();
        loc.add(to);
    }
}
