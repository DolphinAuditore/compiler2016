package AST.Statement.JumpStatement;

import IR.Jump;
import IR.Quadruple;

import java.util.ArrayList;

import static Parser.ClassNameListener.iterlevel;
import static Parser.ClassNameListener.iterstart;
import static Parser.ClassNameListener.loc;

public class Continue extends JumpStatement {

    public Continue() {

    }

    public String name;

    public Continue(String name) {
        this.name = name;
    }

    public void ASTprint() {
        //System.out.println(name);
    }

    public void Final() {
        if (iterlevel == 0) {
            //System.out.println("But no iter here");
            throw new RuntimeException("");
        } else {

        }
    }

    public void IR() throws Exception {
        Jump to = new Jump(iterstart.get(iterlevel - 1));
        loc.add(to);
    }
}
