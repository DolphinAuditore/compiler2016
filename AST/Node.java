package AST;

import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public interface Node {
    void ASTprint();

    void Pre2();

    void Final();

    void IRB(TA ThreeAddress) throws Exception;

    void IR() throws Exception;
}
