package AST.Program;

import AST.Node;
import IR.Quadruple;
import IR.TA;

import java.util.ArrayList;

public class TranslationUnit extends Program{
    public Node lhs, rhs;

    public TranslationUnit(Node lhs, Node rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public void ASTprint() {
        if (lhs != null) lhs.ASTprint();
        System.out.println("-----------------------");
        if (rhs != null) rhs.ASTprint();
    }

    public void Pre2() {
        lhs.Pre2();
        rhs.Pre2();
    }

    public void Final() {
        lhs.Final();
        rhs.Final();
    }

    public void IRB(TA ThreeAddress) throws Exception {
        lhs.IRB(ThreeAddress);
        rhs.IRB(ThreeAddress);
    }

    public void IR() throws Exception {

    }
}
