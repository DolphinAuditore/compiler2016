package IR;

import static Parser.ClassNameListener.writer;

public class Branch extends Quadruple {
    public Address cond;
    public Label lab1;
    public Label lab2;

    public Branch() {
        cond = null;
        lab1 = null;
        lab2 = null;
    }

    public Branch(Address cond, Label lab1, Label lab2) {
        this.cond = cond;
        this.lab1 = lab1;
        this.lab2 = lab2;
    }

    public String toString() {
        return "br " + cond.toString() + " " +
                lab1.toString() + " " + lab2.toString();
    }

    public void setTemp() throws Exception {
        cond.setTemp();
    }

    public void toMIPS() throws Exception {
        new MLoad(0, cond);
        writer.append("bne $t0, $zero, label" + lab1.num + "\n");
        writer.append("beq $t0, $zero, label" + lab2.num + "\n");
    }

}
