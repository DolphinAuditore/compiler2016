package IR;

import static Parser.ClassNameListener.writer;

public class ReturnIR extends Quadruple {
    public Address value;

    public ReturnIR() {
        value = null;
    }

    public ReturnIR(Address value) {
        this.value = value;
    }

    public String toString() {
        return "ret " + value.toString();
    }

    public void setTemp() throws Exception {
        if (value != null) value.setTemp();
    }

    public void toMIPS() throws Exception {
        if (value == null) {

        } else {
            if (value instanceof IntegerConst || value instanceof BooleanConst) {
                writer.append("li $v0, " + value.toMIPS() + "\n");
            } else if (value instanceof StringConst) {
                writer.append("la $v0, " + value.toMIPS() + "\n");
            } else if (value instanceof Temp){
                writer.append("lw $v0, " + value.toMIPS() + "\n");
            } else {
                writer.append(value.getClass().toString());

                writer.append("lw $v0, vvvvvvvvvvvv" + value.toMIPS() + "\n");
            }
        }
        writer.append("jr $ra\n");
    }
}
