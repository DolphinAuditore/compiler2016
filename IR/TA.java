package IR;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

import static Parser.ClassNameListener.TC;
import static Parser.ClassNameListener.writer;

public class TA {
    public ArrayList<Function> fragments;

    public TA() {
        fragments = new ArrayList<Function>();
    }

    public TA(ArrayList<Function> fragments) {
        this.fragments = fragments;
    }

    public void print() throws Exception {
        //System.out.println("Visit TA");
        for(Iterator<Function> it2 = fragments.iterator(); it2.hasNext();) {
            it2.next().print();
        }
    }

    public void toMIPS() throws Exception {
        for(Iterator<Function> it2 = fragments.iterator(); it2.hasNext();) {
            it2.next().toMIPS();
        }
    }
}
