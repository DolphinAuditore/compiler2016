package IR;

import static Parser.ClassNameListener.writer;

public class MemoryStore extends Quadruple {
    public Memory dest;
    public Address src;

    public MemoryStore() {
        dest = null;
        src = null;
    }

    public MemoryStore(Memory dest, Address src) {
        this.dest = dest;
        this.src = src;
    }

    public String toString() {
        return "store 4 " + dest.src.toString() + " " + src.toString() + " " + dest.offset;
    }

    public void setTemp() throws Exception {
        dest.setTemp();
        if (src != null) {
            src.setTemp();
        }
    }

    public void toMIPS() throws Exception {

        new MLoad(1, dest.src);
        //writer.append("lw $t1, " + dest.src.toMIPS() + "\n");
        if (dest.offset != 0) {
            writer.append("add $t1, " + dest.offset + "\n");
        }
        //writer.append("add $t1, " + dest.offset + "\n");
        if (src != null) new MLoad(0, src);
        writer.append("sw $t0, 0($t1)\n");
        /*
        //writer.append("lw $t1, " + dest.src.toMIPS() + "\n");
        //writer.append("add $t1, " + dest.offset + "\n");
        new MLoad(0, src);
        new MStore(0, dest.src);
        //writer.append("sw $t0, " + dest.src.toMIPS() + "\n");
        */
    }
}

