package IR;

import static Parser.ClassNameListener.writer;

public class Jump extends Quadruple {
    public Label label;

    public Jump() {
        label = null;
    }

    public Jump(Label label) {
        this.label = label;
    }

    public String toString() {
        return "jump " + label.toString();
    }

    public void toMIPS() throws Exception {
        writer.append("b " + label.toMIPS() + "\n");
    }
}
