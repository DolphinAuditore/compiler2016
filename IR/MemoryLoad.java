package IR;

import static Parser.ClassNameListener.writer;

public class MemoryLoad extends Quadruple {
    public Memory dest;
    public Address src;

    public MemoryLoad() {
        dest = null;
        src = null;
    }

    public MemoryLoad(Memory dest, Address src) {
        this.dest = dest;
        this.src = src;
    }

    public String toString() {
        return src.toString() + " = load 4 " + dest.src.toString() + " " + dest.offset;
    }

    public void setTemp() throws Exception {
        dest.setTemp();
        src.setTemp();
    }

    public void toMIPS() throws Exception {

        new MLoad(1, dest.src);
        //writer.append("lw $t1, " + dest.src.toMIPS() + "\n");
        if (dest.offset != 0) {
            writer.append("add $t1, " + dest.offset + "\n");
        }
        writer.append("lw $t0, 0($t1)\n");
        new MStore(0, src);

    }
}
