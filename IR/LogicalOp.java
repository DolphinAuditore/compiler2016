package IR;

public enum LogicalOp {
    and, or, not
}
