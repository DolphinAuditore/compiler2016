package IR;

import IR.Temp;
import MIPS.MQ;

import static Parser.ClassNameListener.writer;

/**
 * Created by DolphinAuditore on 2016/5/6 006.
 */

public class MStore extends Quadruple {

    public MStore(Address tmp, Address addr) throws Exception {
        writer.append("sw " + tmp.toMIPS() + ", " + addr.toMIPS() + "\n");
    }

    public MStore(int tt, Address addr) throws Exception {
        writer.append("sw $t" + tt + ", " + addr.toMIPS()  + "\n");
    }
/*
    public void toMIPS() throws Exception {
        writer.append("sw " + tmp.toString() + " " + addr.toString() + "\n");
    }
*/
}

