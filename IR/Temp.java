package IR;

import static Parser.ClassNameListener.*;
import static Symbol.Symbol.symbol;


public class Temp extends Address {
    public static int tempCount = 0;

    public int num;

    public Temp() {
        num = tempCount++;
        this.num = num;
    }

    public Temp(int num) {
        this.num = num;
    }

    public String toString() {
        return "$t" + num + "";
    }

    public void setTemp() throws Exception {
        if (MTM.get(symbol(num)) == null) {
            MTM.put(symbol(num), TC);
            TC -= 4;
        }
        //writer.append(MTM.get(symbol(num)) + "($sp)" +"\n");
        //return src.toString() + " = alloc " + size.toString() + "";
    }

    public String toMIPS() throws Exception {
        if (FullSet.get(symbol(num)) != null) {
            return "T" + (int)FullSet.get(symbol(num));
        } else {
            if (MTM.get(symbol(num)) == null) {
                return "************************ " + num;
            }
            return MTM.get(symbol(num)) + "($sp)";
        }
    }

}
