package IR;

import static Parser.ClassNameListener.MTM;
import static Symbol.Symbol.symbol;

public class IntegerConst extends Const {
    public int value;

    public IntegerConst() {
    }

    public IntegerConst(int value) {
        this.value = value;
    }

    public String toString() {
        return value + "";
    }

    public String toMIPS() throws Exception {
        return value + "";
    }
}
