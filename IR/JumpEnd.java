package IR;

import static Parser.ClassNameListener.writer;

public class JumpEnd extends Quadruple {
    public Label label;

    public JumpEnd() {
        label = null;
    }

    public JumpEnd(Label label) {
        this.label = label;
    }

    public String toString() {
        return label.toString() + ":";
    }

    public void toMIPS() throws Exception {
        writer.append(label.toMIPS() + ":\n");
    }

}
