package IR;

public class BooleanConst extends Const {
    public boolean value;

    public BooleanConst() {
    }

    public BooleanConst(boolean value) {
        this.value = value;
    }

    public String toString() {
        if (value == true) {
            return 1 + "";
        } else {
            return 0 + "";
        }
        //return value + "";
    }

    public String toMIPS() throws Exception {
        if (value == true) {
            return 1 + "";
        } else {
            return 0 + "";
        }
    }
}
