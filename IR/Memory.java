package IR;

import static Parser.ClassNameListener.loc;

public class Memory extends Address {
    public Address src;
    public Temp srcn;
    public int offset;

    public Memory() {
        src = null;
        offset = 0;
    }

    public Memory(Address src, int offset) {
        this.src = src;
        this.offset = offset;
    }

    /*
    public Memory(Address src, Address offset) {
        this.src = src;
        this.offset = offset;
    }
    */

    public Temp MemToTemp() {
        Address sc = src;
        int of = offset;
        if (sc instanceof Memory) {
            sc = ((Memory)sc).MemToTemp();
        }
        Memory sc2 = new Memory(sc, of);
        Temp src3 = new Temp();
        srcn = src3;
        loc.add(new MemoryLoad(sc2, src3));
        return src3;
    }


    public String toString() {
        if (src instanceof Memory) {
            src = ((Memory)src).MemToTemp();
        }
        return src.toString();
    }

    public String toMIPS() {
        return "!@$@#%@$T^%#$^@";
    }

    public void setTemp() throws Exception {
        src.setTemp();
        if (srcn != null) srcn.setTemp();
    }
}
