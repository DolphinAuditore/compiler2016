package IR;

import static Parser.ClassNameListener.MTM;
import static Parser.ClassNameListener.TC;
import static Parser.ClassNameListener.writer;
import static Symbol.Symbol.symbol;

public class Alloc extends Quadruple {
    public Address src;
    public Address size;

    public Alloc() {
        src = null;
        size = null;
    }

    public Alloc(Address src, int size) {
        this.src = src;
        this.size = new IntegerConst(size);
    }

    public Alloc(Address src, Address size) {
        this.src = src;
        this.size = size;
    }

    public String toString() {
        return src.toString() + " = alloc " + size.toString() + "";
    }

    public void setTemp() throws Exception {
        src.setTemp();
        size.setTemp();
        //return src.toString() + " = alloc " + size.toString() + "";
    }

    public void toMIPS() throws Exception {
        if (size instanceof IntegerConst) {
            writer.append("li $a0, " + size.toMIPS() + "\n");
        } else {
            writer.append("lw $a0, " + size.toMIPS() + "\n");
        }
        writer.append("li $v0, 9\n");
        writer.append("syscall\n");
        writer.append("sw $v0, " + src.toMIPS() + "\n");
    }
    /*

li $v0, 9
move $t7, $a0
li $a0, 44
syscall
move $a0, $t7
li $t1, 10
sw $t1, 0($v0)

     */
}
