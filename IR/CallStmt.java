package IR;

import static Parser.ClassNameListener.writer;

public class CallStmt extends Quadruple {
    public CallExpr src;
    public Address src2;

    public CallStmt() {
        src = null;
        src2 = null;
    }

    public CallStmt(CallExpr src) {
        this.src = src;
        src2 = null;
    }

    public CallStmt(CallExpr src, Address src2) {
        this.src = src;
        this.src2 = src2;
    }

    public String toString() {
        if (src2 == null) {
            return src.toString();
        } else {
            return src2.toString() + " = " + src.toString();
        }
    }

    public void setTemp() throws Exception {
        src.setTemp();
        if (src2 != null) {
            src2.setTemp();
        }
    }

    public void toMIPS() throws Exception {
        src.toMIPS();

        if (src2 != null) {
            writer.append("sw $v0, " + src2.toMIPS() + "\n");
            //writer.append("F***********************************K " + src2.getClass() + "\n");

        }
    }
}
