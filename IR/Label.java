package IR;

import static Parser.ClassNameListener.writer;

public class Label extends Address {
    private static int labelCount = 0;

    public int num;

    public Label() {
        num = labelCount++;
    }

    public String toString() {
        return "%label" + num + "";
    }

    public String toMIPS() throws Exception {
        return "label" + num + "";
    }
}
