package IR;

import static Parser.ClassNameListener.ST;
import static Symbol.Symbol.symbol;

public class StringConst extends Const {
    public String str;
    public int num;

    public StringConst() {
    }

    public StringConst(String str, int num) {
        /*
        String AA = "";
        char item;
        for (int i = 0; i < name.length(); i++) {
            item =  name.charAt(i);
            if (item != '\\') {
                AA = AA + item;
            }
            else {
                i++;
                item =  name.charAt(i);
                if (item == 'n') {
                    AA = AA + '\n';
                } else if (item == '\\') {
                    AA = AA + '\\';
                } else if (item == '"') {
                    AA = AA + '"';
                }
            }
            //System.out.println(item);
        }
        */
        this.str = str;
        this.num = num;
    }

    public String toString() {
        return "str" + num;
    }

    public String toMIPS() throws Exception {
        return "str" + num;
    }
}
