package IR;

import static Parser.ClassNameListener.writer;
import static Parser.ClassNameListener.MTM;
import static Parser.ClassNameListener.TC;
import static Symbol.Symbol.symbol;

public class ArithmeticExpr extends Quadruple {
    public ArithmeticOp op;
    public Address dest;
    public Address src1;
    public Address src2;

    public ArithmeticExpr() {
        op = null;
        dest = null;
        src1 = null;
        src2 = null;
    }

    public ArithmeticExpr(Address dest, Address src1, ArithmeticOp op, Address src2) {
        this.dest = dest;
        this.src1 = src1;
        this.op = op;
        this.src2 = src2;
    }

    public ArithmeticExpr(Address dest, ArithmeticOp op, Address src1) {
        this.dest = dest;
        this.op = op;
        this.src1 = src1;
        this.src2 = null;
    }

    public String toString() {
        if (src2 != null) {
            return dest.toString() + " = " + op.toString() + " " + src1.toString() + " " + src2.toString();
        } else {
            return dest.toString() + " = " + op.toString() + " " + src1.toString();
        }
    }

    public void setTemp() throws Exception {
        dest.setTemp();
        src1.setTemp();
        if (src2 != null) {
            src2.setTemp();
        }
        //return src.toString() + " = alloc " + size.toString() + "";
    }

    public void toMIPS() throws Exception {
        if (src2 != null) {
            new MLoad(0, src1);
            new MLoad(1, src2);
            writer.append(op.toString() + " $t0, $t0, $t1\n");
            new MStore(0, dest);
        } else {
            new MLoad(0, src1);
            writer.append(op.toString() + " $t0, $t0\n");
            new MStore(0, dest);
        }
    }
}
