package IR;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

import static Parser.ClassNameListener.*;
import static Symbol.Symbol.symbol;

public class Function {
    public String name;
    public int size;
    public Address param;
    public ArrayList<Quadruple> body;
    //function兼职了全局变量声明功能，param在全局变量声明时即为全局变量的寄存器，此时name为null

    public Function() {
        name = null;
        param = null;
        body = new ArrayList<Quadruple>();
    }
/*
    public Function(String name, ArrayList<Quadruple> body, int size) {
        this.name = name;
        this.param = null;
        this.body = body;
        this.size = size + 132;
    }

    public Function(String name, Address param, ArrayList<Quadruple> body, int size) {
        this.name = name;
        this.param = param;
        this.body = body;
        this.size = size + 132;
    }
*/
    public Function(FuncPart src, ArrayList<Quadruple> body, int size) {
        name = src.name;
        param = src.param;
        this.body = body;
        this.size = size + 144;
        if (name.equals("main")) {

        } else {
            name = name + 44;
        }
        FSN.put(symbol(name), this.size);
    }

    public void setTemp() throws Exception {
        param.setTemp();
        //return src.toString() + " = alloc " + size.toString() + "";
    }
/*
    public void print() {
        if (name == null) {
            for (Iterator it2 = body.iterator(); it2.hasNext(); ) {
                System.out.println(it2.next().toString());
            }
        } else {
            if (param == null) {
                System.out.println("func " + name + " {");
            } else {
                System.out.println("func " + name + " " + param.toString() + " {");
            }
            System.out.println("%" + name + "_entry:");
            for (Iterator it2 = body.iterator(); it2.hasNext(); ) {
                System.out.println(it2.next().toString());
            }
            System.out.println("}");
        }
    }
*/

    //file out
    public void print() throws Exception {
        if (name == null) {
            for (Iterator it2 = body.iterator(); it2.hasNext(); ) {
                writer.append(it2.next().toString() + "\n");
            }
        } else {
            if (param == null) {
                writer.append("func " + name + " {\n");
            } else {
                writer.append("func " + name + " " + param.toString() + " {\n");
            }
            writer.append("%" + name + "_entry:\n");
            if (name.equals("main")) {
                for (Iterator<Quadruple> it2 = fullvar.iterator(); it2.hasNext(); ) {
                    writer.append(it2.next().toString() + "\n");
                    //fullvar.add(it2.next());
                    //writer.append(it2.next().toString() + "\n");
                }
            }
            for (Iterator it2 = body.iterator(); it2.hasNext(); ) {
                writer.append(it2.next().toString() + "\n");
            }
            writer.append("}\n");
        }
    }

    public void toMIPS() throws Exception {
        writer.append(name + ":\n");
        //writer.append("sSSSSSSSSSSSSSSSSSSSS " + size + "\n");
        /*
        if (param == null) {
            writer.append(name + ":\n");
        } else {
            writer.append("func " + name + " " + param.toString() + " {\n");
        }*/

        TC = size - 4;
        if (param != null) param.setTemp();
        if (name.equals("main")) {
            size += Extra;
            TC = size - 4;
            for (Iterator<Quadruple> it2 = fullvar.iterator(); it2.hasNext(); ) {
                it2.next().setTemp();
            }
        }

        for (Iterator<Quadruple> it2 = body.iterator(); it2.hasNext(); ) {
            it2.next().setTemp();
        }

        if (name.equals("main")) {
            writer.append("sub $sp, $sp, " + size + "\n");
        }

        if (name.equals("main")) {
            for (Iterator<Quadruple> it2 = fullvar.iterator(); it2.hasNext(); ) {
                it2.next().toMIPS();
                //writer.append(it2.next().toMIPS() + "\n");
                //fullvar.add(it2.next());
                //writer.append(it2.next().toString() + "\n");
            }
        }
        //writer.append("sw $ra, 136($sp)\n");
        //writer.append("lw $ra, 136($sp)\n");

        for (Iterator<Quadruple> it2 = body.iterator(); it2.hasNext(); ) {
            it2.next().toMIPS();
        }

        if (name.equals("main")) {
            writer.append("add $sp, $sp, " + size + "\n");
        }

        writer.append("jr $ra\n");
    }
}
