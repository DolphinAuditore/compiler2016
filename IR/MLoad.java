package IR;

import IR.Address;
import IR.Temp;
import MIPS.MQ;

import java.net.Inet4Address;

import static Parser.ClassNameListener.FullSet;
import static Parser.ClassNameListener.writer;
import static Symbol.Symbol.symbol;

/**
 * Created by DolphinAuditore on 2016/5/6 006.
 */

public class MLoad extends MQ {
    //Address tmp;
    //Address addr;

    public MLoad(Address tmp, Address addr) throws Exception {
        //this.tmp = tmp;
        //this.addr = addr;
        if (tmp instanceof Temp) {
            writer.append("lw " + tmp.toMIPS() + ", " + addr.toMIPS() + "\n");
        } else {
            writer.append("li " + tmp.toMIPS() + ", " + addr.toMIPS() + "\n");
        }
    }

    public MLoad(int tt, Address addr) throws Exception {
        //this.tmp = tmp;
        if (addr instanceof Temp) {
            writer.append("lw $t" + tt + ", " + addr.toMIPS() + "\n");
        } else if (addr instanceof IntegerConst || addr instanceof BooleanConst) {
            writer.append("li $t" + tt + ", " + addr.toMIPS() + "\n");
        } else {
            //writer.append(addr.getClass()+"\n");
            writer.append("la $t" + tt + ", " + addr.toMIPS() + "\n");
        }
    }
    /*
    public void toMIPS() {
        if (tmp instanceof Temp) {
            return "lw " + tmp.toString() + " " + addr.toString();
        } else {
            return "li " + tmp.toString() + " " + addr.toString();
        }
    }
    */
}

