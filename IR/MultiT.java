package IR;

public class MultiT extends Address {
    public Address srclist;
    public Address src;

    public MultiT() {
        srclist = null;
        src = null;
    }

    public MultiT(Address srclist, Address src) {
        this.srclist = srclist;
        this.src = src;
    }

    public String toString() {
        return srclist.toString() + " " + src.toString();
    }

    public void setTemp() throws Exception {
        srclist.setTemp();
        src.setTemp();
    }

    public String MultiT() throws Exception {
        return "";
    }
}
