package IR;

import static Parser.ClassNameListener.*;
import static Symbol.Symbol.symbol;

public class CallExpr extends Address {
    public String name;
    public Address args;

    public CallExpr() {
        args = null;
    }

    public CallExpr(String name, Address args) {
        this.name = name;
        this.args = args;
    }

    public CallExpr(String name) {
        this.name = name;
        this.args = null;
    }

    public String toString() {
        if (args == null) {
            return "call " + name;
        } else {
            return "call " + name + " " + args.toString();
        }
    }

    public void setTemp() throws Exception {
        if (args != null) args.setTemp();
    }

    public String toMIPS() throws Exception {
        writer.append("sw $ra, 136($sp)\n");
        if (name.equals("func_getInt")) {
            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_getString")) {
            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_toString")) {
            writer.append("lw $a0, " + args.toMIPS() + " \n");
            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_print")) {
            if (args instanceof StringConst) {
                writer.append("la $a0, " + args.toMIPS() + "\n");
            } else {
                writer.append("lw $a0, " + args.toMIPS() + "\n");
            }
            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_println")) {
            if (args instanceof StringConst) {
                writer.append("la $a0, " + args.toMIPS() + "\n");
            } else {
                writer.append("lw $a0, " + args.toMIPS() + "\n");
            }
            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_stringConcatenate")) {

            if (((MultiT)args).srclist instanceof StringConst) {
                writer.append("la $a0, " + ((MultiT)args).srclist.toMIPS() + "\n");
            } else {
                writer.append("lw $a0, " + ((MultiT)args).srclist.toMIPS() + "\n");
            }

            if (((MultiT)args).src instanceof StringConst) {
                writer.append("la $a1, " + ((MultiT)args).src.toMIPS() + "\n");
            } else {
                writer.append("lw $a1, " + ((MultiT)args).src.toMIPS() + "\n");
            }

            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_stringIsEqual")) {
            if (((MultiT) args).srclist instanceof StringConst) {
                writer.append("la $a0, " + ((MultiT) args).srclist.toMIPS() + "\n");
            } else {
                writer.append("lw $a0, " + ((MultiT) args).srclist.toMIPS() + "\n");
            }

            if (((MultiT) args).src instanceof StringConst) {
                writer.append("la $a1, " + ((MultiT) args).src.toMIPS() + "\n");
            } else {
                writer.append("lw $a1, " + ((MultiT) args).src.toMIPS() + "\n");
            }

            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_stringLess")) {
            if (((MultiT) args).srclist instanceof StringConst) {
                writer.append("la $a0, " + ((MultiT) args).srclist.toMIPS() + "\n");
            } else {
                writer.append("lw $a0, " + ((MultiT) args).srclist.toMIPS() + "\n");
            }

            if (((MultiT) args).src instanceof StringConst) {
                writer.append("la $a1, " + ((MultiT) args).src.toMIPS() + "\n");
            } else {
                writer.append("lw $a1, " + ((MultiT) args).src.toMIPS() + "\n");
            }

            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_string.length")) {
            if (args instanceof StringConst) {
                writer.append("la $a0, " + args.toMIPS() + "\n");
            } else {
                writer.append("lw $a0, " + args.toMIPS() + "\n");
            }
            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_string.parseInt")) {
            if (args instanceof StringConst) {
                writer.append("la $a0, " + args.toMIPS() + "\n");
            } else {
                writer.append("lw $a0, " + args.toMIPS() + "\n");
            }
            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_string.ord")) {
            Address args1 = ((MultiT)args).srclist;
            Address args2 = ((MultiT)args).src;
            if (args1 instanceof StringConst) {
                writer.append("la $a0, " + args1.toMIPS() + "\n");
            } else {
                writer.append("lw $a0, " + args1.toMIPS() + "\n");
            }
            if (args2 instanceof IntegerConst) {
                writer.append("li $a1, " + args2.toMIPS() + "\n");
            } else {
                writer.append("lw $a1, " + args2.toMIPS() + "\n");
            }
            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        } else if (name.equals("func_string.substring")) {
            Address args1 = ((MultiT)args).srclist;
            Address args2 = ((MultiT)((MultiT)args).src).srclist;
            Address args3 = ((MultiT)((MultiT)args).src).src;
            if (args1 instanceof StringConst) {
                writer.append("la $a0, " + args1.toMIPS() + "\n");
            } else {
                writer.append("lw $a0, " + args1.toMIPS() + "\n");
            }

            if (args2 instanceof IntegerConst) {
                writer.append("li $a1, " + args2.toMIPS() + "\n");
            } else {
                writer.append("lw $a1, " + args2.toMIPS() + "\n");
            }

            if (args3 instanceof IntegerConst) {
                writer.append("li $a2, " + args3.toMIPS() + "\n");
            } else {
                writer.append("lw $a2, " + args3.toMIPS() + "\n");
            }

            writer.append("jal " + name + " \n");
            writer.append("lw $ra, 136($sp)\n");
            return "";
        }

        int size = (int)FSN.get(symbol(name));
        //writer.append("SIZZEEEEEEEEEEEEEEEEEE " + size + "\n");
        //writer.append("sw $ra, 136($sp)\n");
        writer.append("sub $sp, $sp, " + size + "\n");
        int tc = size - 4;
        Address tar = args;
        Address sr;

        while (tar instanceof MultiT) {
            sr = ((MultiT)tar).src;
            tc -= 4;
            tar = ((MultiT)tar).srclist;
        }

        tar = args;
        while (tar instanceof MultiT) {
            sr = ((MultiT)tar).src;
            //new MLoad(0, ((MultiT)tar).src);

            if (sr instanceof Temp) {
                if (FullSet.get(symbol(((Temp)sr).num)) == null) {
                    writer.append("lw $t0, " + ((int) MTM.get(symbol(((Temp) sr).num)) + size) + "($sp)\n");
                } else {
                    writer.append("lw $t0, " + sr.toMIPS() + "\n");
                }
            } else if (sr instanceof IntegerConst || sr instanceof BooleanConst){
                writer.append("li $t0, " + sr.toMIPS() + "\n");
            } else {
                writer.append("la $t0, " + sr.toMIPS() + "\n");
            }

            writer.append("sw $t0, " + tc + "($sp)\n");
            tc += 4;
            tar = ((MultiT)tar).srclist;
        }
        //new MLoad(0, tar);
        sr = tar;
        if (sr instanceof Temp) {
            if (FullSet.get(symbol(((Temp)sr).num)) == null) {
                writer.append("lw $t0, " + ((int) MTM.get(symbol(((Temp) sr).num)) + size) + "($sp)\n");
            } else {
                writer.append("lw $t0, " + sr.toMIPS() + "\n");
            }
        } else if (sr != null) {
            if (sr instanceof IntegerConst || sr instanceof BooleanConst){
                writer.append("li $t0, " + sr.toMIPS() + "\n");
            } else {
                writer.append("la $t0, " + sr.toMIPS() + "\n");
            }
        }

        writer.append("sw $t0, " + tc + "($sp)\n");
        tc += 4;

        writer.append("jal " + name + " \n");

        writer.append("add $sp, $sp, " + size + "\n");

        writer.append("lw $ra, 136($sp)\n");
        return "";
    }
}
