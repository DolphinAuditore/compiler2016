package IR;

import static Parser.ClassNameListener.writer;

public class Assign extends Quadruple {
    public Address dest;
    public Address src;

    public Assign() {
        dest = null;
        src = null;
    }

    public Assign(Address dest, Address src) {
        this.dest = dest;
        this.src = src;
    }

    public String toString() {
        return dest.toString() + " = move " + src.toString();
    }

    public void setTemp() throws Exception {
        dest.setTemp();
        if (src != null) {
            src.setTemp();
        }
    }

    public void toMIPS() throws Exception {
        if (src != null) new MLoad(0, src);
        new MStore(0, dest);
    }
}
