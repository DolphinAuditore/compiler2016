package IR;

import java.util.ArrayList;
import java.util.Iterator;

public class FuncPart extends Address {
    public String name;
    public Address param;

    public FuncPart() {
        name = null;
        param = null;
    }

    public FuncPart(String name) {
        this.name = name;
        this.param = null;
    }

    public FuncPart(String name, Address param) {
        this.name = name;
        this.param = param;
    }

    public String toString() {
        return "";
    }

    public String toMIPS() {
        return "ASFDKJALFKALDFKSLDFKL";
    }

    public void setTemp() throws Exception {
        param.setTemp();
    }
}
