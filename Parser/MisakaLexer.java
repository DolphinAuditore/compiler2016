// Generated from C:/Users/DolphinAuditore/workspace/Misaka/src/Parser\Misaka.g4 by ANTLR 4.5.1
package Parser;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MisakaLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NULL=1, Break=2, Continue=3, Else=4, For=5, If=6, Int=7, Bool=8, String=9, 
		New=10, Return=11, Class=12, Void=13, While=14, LP=15, RP=16, LK=17, RK=18, 
		LB=19, RB=20, Less=21, LessEqual=22, Greater=23, GreaterEqual=24, LeftShift=25, 
		RightShift=26, Plus=27, PlusPlus=28, Minus=29, MinusMinus=30, Star=31, 
		Div=32, Mod=33, And=34, Or=35, AndAnd=36, OrOr=37, Caret=38, Not=39, Tilde=40, 
		Question=41, Colon=42, Semi=43, Comma=44, Assign=45, Equal=46, NotEqual=47, 
		Dot=48, BOOL_LITERAL=49, ID=50, INT_LITERAL=51, STRING_LITERAL=52, Whitespace=53, 
		Newline=54, BlockComment=55, LineComment=56;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"NULL", "Break", "Continue", "Else", "For", "If", "Int", "Bool", "String", 
		"New", "Return", "Class", "Void", "While", "LP", "RP", "LK", "RK", "LB", 
		"RB", "Less", "LessEqual", "Greater", "GreaterEqual", "LeftShift", "RightShift", 
		"Plus", "PlusPlus", "Minus", "MinusMinus", "Star", "Div", "Mod", "And", 
		"Or", "AndAnd", "OrOr", "Caret", "Not", "Tilde", "TRUE", "FALSE", "Question", 
		"Colon", "Semi", "Comma", "Assign", "Equal", "NotEqual", "Dot", "BOOL_LITERAL", 
		"ID", "Nondigit", "Digit", "INT_LITERAL", "NonzeroDigit", "Sign", "Digit_seq", 
		"STRING_LITERAL", "SChar_seq", "SChar", "Escape_seq", "Whitespace", "Newline", 
		"BlockComment", "LineComment"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'null'", "'break'", "'continue'", "'else'", "'for'", "'if'", "'int'", 
		"'bool'", "'string'", "'new'", "'return'", "'class'", "'void'", "'while'", 
		"'('", "')'", "'['", "']'", "'{'", "'}'", "'<'", "'<='", "'>'", "'>='", 
		"'<<'", "'>>'", "'+'", "'++'", "'-'", "'--'", "'*'", "'/'", "'%'", "'&'", 
		null, "'&&'", "'||'", "'^'", "'!'", "'~'", "'?'", "':'", "';'", "','", 
		"'='", "'=='", "'!='", "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "NULL", "Break", "Continue", "Else", "For", "If", "Int", "Bool", 
		"String", "New", "Return", "Class", "Void", "While", "LP", "RP", "LK", 
		"RK", "LB", "RB", "Less", "LessEqual", "Greater", "GreaterEqual", "LeftShift", 
		"RightShift", "Plus", "PlusPlus", "Minus", "MinusMinus", "Star", "Div", 
		"Mod", "And", "Or", "AndAnd", "OrOr", "Caret", "Not", "Tilde", "Question", 
		"Colon", "Semi", "Comma", "Assign", "Equal", "NotEqual", "Dot", "BOOL_LITERAL", 
		"ID", "INT_LITERAL", "STRING_LITERAL", "Whitespace", "Newline", "BlockComment", 
		"LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MisakaLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Misaka.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2:\u019f\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6"+
		"\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r"+
		"\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26"+
		"\3\26\3\27\3\27\3\27\3\30\3\30\3\31\3\31\3\31\3\32\3\32\3\32\3\33\3\33"+
		"\3\33\3\34\3\34\3\35\3\35\3\35\3\36\3\36\3\37\3\37\3\37\3 \3 \3!\3!\3"+
		"\"\3\"\3#\3#\3$\3$\3%\3%\3%\3&\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\3*\3*\3"+
		"*\3+\3+\3+\3+\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\61\3"+
		"\62\3\62\3\62\3\63\3\63\3\64\3\64\5\64\u012f\n\64\3\65\3\65\3\65\7\65"+
		"\u0134\n\65\f\65\16\65\u0137\13\65\3\66\3\66\3\67\3\67\38\38\78\u013f"+
		"\n8\f8\168\u0142\138\38\58\u0145\n8\39\39\3:\3:\3;\6;\u014c\n;\r;\16;"+
		"\u014d\3<\3<\5<\u0152\n<\3<\3<\3=\6=\u0157\n=\r=\16=\u0158\3>\3>\5>\u015d"+
		"\n>\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?"+
		"\5?\u0175\n?\3@\6@\u0178\n@\r@\16@\u0179\3@\3@\3A\3A\5A\u0180\nA\3A\5"+
		"A\u0183\nA\3A\3A\3B\3B\3B\3B\7B\u018b\nB\fB\16B\u018e\13B\3B\3B\3B\3B"+
		"\3B\3C\3C\3C\3C\7C\u0199\nC\fC\16C\u019c\13C\3C\3C\3\u018c\2D\3\3\5\4"+
		"\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22"+
		"#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C"+
		"#E$G%I&K\'M(O)Q*S\2U\2W+Y,[-]._/a\60c\61e\62g\63i\64k\2m\2o\65q\2s\2u"+
		"\2w\66y\2{\2}\2\177\67\u00818\u00839\u0085:\3\2\t\5\2C\\aac|\3\2\62;\3"+
		"\2\63;\4\2--//\6\2\f\f\17\17$$^^\4\2\13\13\"\"\4\2\f\f\17\17\u01ac\2\3"+
		"\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2"+
		"\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31"+
		"\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2"+
		"\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2"+
		"\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2"+
		"\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2"+
		"I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2W\3\2\2\2\2Y\3"+
		"\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2"+
		"\2\2g\3\2\2\2\2i\3\2\2\2\2o\3\2\2\2\2w\3\2\2\2\2\177\3\2\2\2\2\u0081\3"+
		"\2\2\2\2\u0083\3\2\2\2\2\u0085\3\2\2\2\3\u0087\3\2\2\2\5\u008c\3\2\2\2"+
		"\7\u0092\3\2\2\2\t\u009b\3\2\2\2\13\u00a0\3\2\2\2\r\u00a4\3\2\2\2\17\u00a7"+
		"\3\2\2\2\21\u00ab\3\2\2\2\23\u00b0\3\2\2\2\25\u00b7\3\2\2\2\27\u00bb\3"+
		"\2\2\2\31\u00c2\3\2\2\2\33\u00c8\3\2\2\2\35\u00cd\3\2\2\2\37\u00d3\3\2"+
		"\2\2!\u00d5\3\2\2\2#\u00d7\3\2\2\2%\u00d9\3\2\2\2\'\u00db\3\2\2\2)\u00dd"+
		"\3\2\2\2+\u00df\3\2\2\2-\u00e1\3\2\2\2/\u00e4\3\2\2\2\61\u00e6\3\2\2\2"+
		"\63\u00e9\3\2\2\2\65\u00ec\3\2\2\2\67\u00ef\3\2\2\29\u00f1\3\2\2\2;\u00f4"+
		"\3\2\2\2=\u00f6\3\2\2\2?\u00f9\3\2\2\2A\u00fb\3\2\2\2C\u00fd\3\2\2\2E"+
		"\u00ff\3\2\2\2G\u0101\3\2\2\2I\u0103\3\2\2\2K\u0106\3\2\2\2M\u0109\3\2"+
		"\2\2O\u010b\3\2\2\2Q\u010d\3\2\2\2S\u010f\3\2\2\2U\u0114\3\2\2\2W\u011a"+
		"\3\2\2\2Y\u011c\3\2\2\2[\u011e\3\2\2\2]\u0120\3\2\2\2_\u0122\3\2\2\2a"+
		"\u0124\3\2\2\2c\u0127\3\2\2\2e\u012a\3\2\2\2g\u012e\3\2\2\2i\u0130\3\2"+
		"\2\2k\u0138\3\2\2\2m\u013a\3\2\2\2o\u0144\3\2\2\2q\u0146\3\2\2\2s\u0148"+
		"\3\2\2\2u\u014b\3\2\2\2w\u014f\3\2\2\2y\u0156\3\2\2\2{\u015c\3\2\2\2}"+
		"\u0174\3\2\2\2\177\u0177\3\2\2\2\u0081\u0182\3\2\2\2\u0083\u0186\3\2\2"+
		"\2\u0085\u0194\3\2\2\2\u0087\u0088\7p\2\2\u0088\u0089\7w\2\2\u0089\u008a"+
		"\7n\2\2\u008a\u008b\7n\2\2\u008b\4\3\2\2\2\u008c\u008d\7d\2\2\u008d\u008e"+
		"\7t\2\2\u008e\u008f\7g\2\2\u008f\u0090\7c\2\2\u0090\u0091\7m\2\2\u0091"+
		"\6\3\2\2\2\u0092\u0093\7e\2\2\u0093\u0094\7q\2\2\u0094\u0095\7p\2\2\u0095"+
		"\u0096\7v\2\2\u0096\u0097\7k\2\2\u0097\u0098\7p\2\2\u0098\u0099\7w\2\2"+
		"\u0099\u009a\7g\2\2\u009a\b\3\2\2\2\u009b\u009c\7g\2\2\u009c\u009d\7n"+
		"\2\2\u009d\u009e\7u\2\2\u009e\u009f\7g\2\2\u009f\n\3\2\2\2\u00a0\u00a1"+
		"\7h\2\2\u00a1\u00a2\7q\2\2\u00a2\u00a3\7t\2\2\u00a3\f\3\2\2\2\u00a4\u00a5"+
		"\7k\2\2\u00a5\u00a6\7h\2\2\u00a6\16\3\2\2\2\u00a7\u00a8\7k\2\2\u00a8\u00a9"+
		"\7p\2\2\u00a9\u00aa\7v\2\2\u00aa\20\3\2\2\2\u00ab\u00ac\7d\2\2\u00ac\u00ad"+
		"\7q\2\2\u00ad\u00ae\7q\2\2\u00ae\u00af\7n\2\2\u00af\22\3\2\2\2\u00b0\u00b1"+
		"\7u\2\2\u00b1\u00b2\7v\2\2\u00b2\u00b3\7t\2\2\u00b3\u00b4\7k\2\2\u00b4"+
		"\u00b5\7p\2\2\u00b5\u00b6\7i\2\2\u00b6\24\3\2\2\2\u00b7\u00b8\7p\2\2\u00b8"+
		"\u00b9\7g\2\2\u00b9\u00ba\7y\2\2\u00ba\26\3\2\2\2\u00bb\u00bc\7t\2\2\u00bc"+
		"\u00bd\7g\2\2\u00bd\u00be\7v\2\2\u00be\u00bf\7w\2\2\u00bf\u00c0\7t\2\2"+
		"\u00c0\u00c1\7p\2\2\u00c1\30\3\2\2\2\u00c2\u00c3\7e\2\2\u00c3\u00c4\7"+
		"n\2\2\u00c4\u00c5\7c\2\2\u00c5\u00c6\7u\2\2\u00c6\u00c7\7u\2\2\u00c7\32"+
		"\3\2\2\2\u00c8\u00c9\7x\2\2\u00c9\u00ca\7q\2\2\u00ca\u00cb\7k\2\2\u00cb"+
		"\u00cc\7f\2\2\u00cc\34\3\2\2\2\u00cd\u00ce\7y\2\2\u00ce\u00cf\7j\2\2\u00cf"+
		"\u00d0\7k\2\2\u00d0\u00d1\7n\2\2\u00d1\u00d2\7g\2\2\u00d2\36\3\2\2\2\u00d3"+
		"\u00d4\7*\2\2\u00d4 \3\2\2\2\u00d5\u00d6\7+\2\2\u00d6\"\3\2\2\2\u00d7"+
		"\u00d8\7]\2\2\u00d8$\3\2\2\2\u00d9\u00da\7_\2\2\u00da&\3\2\2\2\u00db\u00dc"+
		"\7}\2\2\u00dc(\3\2\2\2\u00dd\u00de\7\177\2\2\u00de*\3\2\2\2\u00df\u00e0"+
		"\7>\2\2\u00e0,\3\2\2\2\u00e1\u00e2\7>\2\2\u00e2\u00e3\7?\2\2\u00e3.\3"+
		"\2\2\2\u00e4\u00e5\7@\2\2\u00e5\60\3\2\2\2\u00e6\u00e7\7@\2\2\u00e7\u00e8"+
		"\7?\2\2\u00e8\62\3\2\2\2\u00e9\u00ea\7>\2\2\u00ea\u00eb\7>\2\2\u00eb\64"+
		"\3\2\2\2\u00ec\u00ed\7@\2\2\u00ed\u00ee\7@\2\2\u00ee\66\3\2\2\2\u00ef"+
		"\u00f0\7-\2\2\u00f08\3\2\2\2\u00f1\u00f2\7-\2\2\u00f2\u00f3\7-\2\2\u00f3"+
		":\3\2\2\2\u00f4\u00f5\7/\2\2\u00f5<\3\2\2\2\u00f6\u00f7\7/\2\2\u00f7\u00f8"+
		"\7/\2\2\u00f8>\3\2\2\2\u00f9\u00fa\7,\2\2\u00fa@\3\2\2\2\u00fb\u00fc\7"+
		"\61\2\2\u00fcB\3\2\2\2\u00fd\u00fe\7\'\2\2\u00feD\3\2\2\2\u00ff\u0100"+
		"\7(\2\2\u0100F\3\2\2\2\u0101\u0102\5)\25\2\u0102H\3\2\2\2\u0103\u0104"+
		"\7(\2\2\u0104\u0105\7(\2\2\u0105J\3\2\2\2\u0106\u0107\7~\2\2\u0107\u0108"+
		"\7~\2\2\u0108L\3\2\2\2\u0109\u010a\7`\2\2\u010aN\3\2\2\2\u010b\u010c\7"+
		"#\2\2\u010cP\3\2\2\2\u010d\u010e\7\u0080\2\2\u010eR\3\2\2\2\u010f\u0110"+
		"\7v\2\2\u0110\u0111\7t\2\2\u0111\u0112\7w\2\2\u0112\u0113\7g\2\2\u0113"+
		"T\3\2\2\2\u0114\u0115\7h\2\2\u0115\u0116\7c\2\2\u0116\u0117\7n\2\2\u0117"+
		"\u0118\7u\2\2\u0118\u0119\7g\2\2\u0119V\3\2\2\2\u011a\u011b\7A\2\2\u011b"+
		"X\3\2\2\2\u011c\u011d\7<\2\2\u011dZ\3\2\2\2\u011e\u011f\7=\2\2\u011f\\"+
		"\3\2\2\2\u0120\u0121\7.\2\2\u0121^\3\2\2\2\u0122\u0123\7?\2\2\u0123`\3"+
		"\2\2\2\u0124\u0125\7?\2\2\u0125\u0126\7?\2\2\u0126b\3\2\2\2\u0127\u0128"+
		"\7#\2\2\u0128\u0129\7?\2\2\u0129d\3\2\2\2\u012a\u012b\7\60\2\2\u012bf"+
		"\3\2\2\2\u012c\u012f\5S*\2\u012d\u012f\5U+\2\u012e\u012c\3\2\2\2\u012e"+
		"\u012d\3\2\2\2\u012fh\3\2\2\2\u0130\u0135\5k\66\2\u0131\u0134\5k\66\2"+
		"\u0132\u0134\5m\67\2\u0133\u0131\3\2\2\2\u0133\u0132\3\2\2\2\u0134\u0137"+
		"\3\2\2\2\u0135\u0133\3\2\2\2\u0135\u0136\3\2\2\2\u0136j\3\2\2\2\u0137"+
		"\u0135\3\2\2\2\u0138\u0139\t\2\2\2\u0139l\3\2\2\2\u013a\u013b\t\3\2\2"+
		"\u013bn\3\2\2\2\u013c\u0140\5q9\2\u013d\u013f\5m\67\2\u013e\u013d\3\2"+
		"\2\2\u013f\u0142\3\2\2\2\u0140\u013e\3\2\2\2\u0140\u0141\3\2\2\2\u0141"+
		"\u0145\3\2\2\2\u0142\u0140\3\2\2\2\u0143\u0145\7\62\2\2\u0144\u013c\3"+
		"\2\2\2\u0144\u0143\3\2\2\2\u0145p\3\2\2\2\u0146\u0147\t\4\2\2\u0147r\3"+
		"\2\2\2\u0148\u0149\t\5\2\2\u0149t\3\2\2\2\u014a\u014c\5m\67\2\u014b\u014a"+
		"\3\2\2\2\u014c\u014d\3\2\2\2\u014d\u014b\3\2\2\2\u014d\u014e\3\2\2\2\u014e"+
		"v\3\2\2\2\u014f\u0151\7$\2\2\u0150\u0152\5y=\2\u0151\u0150\3\2\2\2\u0151"+
		"\u0152\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0154\7$\2\2\u0154x\3\2\2\2\u0155"+
		"\u0157\5{>\2\u0156\u0155\3\2\2\2\u0157\u0158\3\2\2\2\u0158\u0156\3\2\2"+
		"\2\u0158\u0159\3\2\2\2\u0159z\3\2\2\2\u015a\u015d\n\6\2\2\u015b\u015d"+
		"\5}?\2\u015c\u015a\3\2\2\2\u015c\u015b\3\2\2\2\u015d|\3\2\2\2\u015e\u015f"+
		"\7^\2\2\u015f\u0175\7)\2\2\u0160\u0161\7^\2\2\u0161\u0175\7$\2\2\u0162"+
		"\u0163\7^\2\2\u0163\u0175\7A\2\2\u0164\u0165\7^\2\2\u0165\u0175\7^\2\2"+
		"\u0166\u0167\7^\2\2\u0167\u0175\7c\2\2\u0168\u0169\7^\2\2\u0169\u0175"+
		"\7d\2\2\u016a\u016b\7^\2\2\u016b\u0175\7h\2\2\u016c\u016d\7^\2\2\u016d"+
		"\u0175\7p\2\2\u016e\u016f\7^\2\2\u016f\u0175\7t\2\2\u0170\u0171\7^\2\2"+
		"\u0171\u0175\7v\2\2\u0172\u0173\7^\2\2\u0173\u0175\7x\2\2\u0174\u015e"+
		"\3\2\2\2\u0174\u0160\3\2\2\2\u0174\u0162\3\2\2\2\u0174\u0164\3\2\2\2\u0174"+
		"\u0166\3\2\2\2\u0174\u0168\3\2\2\2\u0174\u016a\3\2\2\2\u0174\u016c\3\2"+
		"\2\2\u0174\u016e\3\2\2\2\u0174\u0170\3\2\2\2\u0174\u0172\3\2\2\2\u0175"+
		"~\3\2\2\2\u0176\u0178\t\7\2\2\u0177\u0176\3\2\2\2\u0178\u0179\3\2\2\2"+
		"\u0179\u0177\3\2\2\2\u0179\u017a\3\2\2\2\u017a\u017b\3\2\2\2\u017b\u017c"+
		"\b@\2\2\u017c\u0080\3\2\2\2\u017d\u017f\7\17\2\2\u017e\u0180\7\f\2\2\u017f"+
		"\u017e\3\2\2\2\u017f\u0180\3\2\2\2\u0180\u0183\3\2\2\2\u0181\u0183\7\f"+
		"\2\2\u0182\u017d\3\2\2\2\u0182\u0181\3\2\2\2\u0183\u0184\3\2\2\2\u0184"+
		"\u0185\bA\2\2\u0185\u0082\3\2\2\2\u0186\u0187\7\61\2\2\u0187\u0188\7,"+
		"\2\2\u0188\u018c\3\2\2\2\u0189\u018b\13\2\2\2\u018a\u0189\3\2\2\2\u018b"+
		"\u018e\3\2\2\2\u018c\u018d\3\2\2\2\u018c\u018a\3\2\2\2\u018d\u018f\3\2"+
		"\2\2\u018e\u018c\3\2\2\2\u018f\u0190\7,\2\2\u0190\u0191\7\61\2\2\u0191"+
		"\u0192\3\2\2\2\u0192\u0193\bB\2\2\u0193\u0084\3\2\2\2\u0194\u0195\7\61"+
		"\2\2\u0195\u0196\7\61\2\2\u0196\u019a\3\2\2\2\u0197\u0199\n\b\2\2\u0198"+
		"\u0197\3\2\2\2\u0199\u019c\3\2\2\2\u019a\u0198\3\2\2\2\u019a\u019b\3\2"+
		"\2\2\u019b\u019d\3\2\2\2\u019c\u019a\3\2\2\2\u019d\u019e\bC\2\2\u019e"+
		"\u0086\3\2\2\2\22\2\u012e\u0133\u0135\u0140\u0144\u014d\u0151\u0158\u015c"+
		"\u0174\u0179\u017f\u0182\u018c\u019a\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}