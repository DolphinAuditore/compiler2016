all: clean
	@mkdir -p ./bin
	@javac -cp \
		./antlr-4.5-complete.jar\
		./*/*/*/*/*.java \
		./*/*/*/*.java \
		./*/*/*.java \
		./*/*.java \
		./*.java \
	-d ./bin
	@cp ./build_in_functions.s ./bin
	@cp ./antlr-4.5-complete.jar ./bin
	@cd ./bin && jar xf ./antlr-4.5-complete.jar \
			  && rm -rf ./META-INF \
			  && jar cef Main Compiler.jar ./

clean:
	rm -rf ./bin
